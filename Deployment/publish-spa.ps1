[DateTime]$date2000 = New-Object DateTime(2000, 1, 1)
[DateTime]$today = ([DateTime]::UtcNow).Date
[int]$daysSince2000 = [Math]::Floor(([DateTime]::UtcNow).Subtract($date2000).TotalDays)
[int]$msDiv2FromMidnight = ([DateTime]::UtcNow).Subtract($today).TotalSeconds / 2
$version = [String]::Format('$1$2.$3.{0}.{1}$6', $daysSince2000, $msDiv2FromMidnight)  
(Get-Content "FirstQuad.DynamicTree.SPA.nuspec") | Foreach-Object {$_ -replace "(<version>)([0-9]+).([0-9]+).([0-9]+).([0-9]+)(<\/version>)", $version} | Set-Content "FirstQuad.DynamicTree.SPA.nuspec"

Remove-Item * -Filter *.nupkg 
$nuget = Resolve-Path "nuget.exe"
&$nuget pack FirstQuad.DynamicTree.SPA.nuspec
&$nuget push *.nupkg ecdfe2ed-e5a9-43d8-a9cd-d0789caf88d2 -s https://www.nuget.org/api/v2/package
Remove-Item * -Filter *.nupkg 
