﻿/* Part of nuget package */
function DynamicTree(selector, options) {
    var $ = window.jQuery;
    if (typeof $ == "undefined") throw Error("jQuery is required");

    var self = $(selector);
    var totalItemsCount = 0;
    var ascendingOrder = "_ASC";
    var descendingOrder = "_DESC";
    var vkEnter = 13;
    var vkSpace = 32;
    var vkLeft = 37;
    var vkUp = 38;
    var vkRight = 39;
    var vkDown = 40;

    var o = $.extend({
        autoRender: true,
        showCheckAll: false,
        showCheckBoxes: false,
        isReadOnly: false,
        singleSelection: false,
        recursiveSelection: true,
        propagateSelectionToParents: true,
        checkByClick: false,
        useForm: false,
        allowColumnReordering: false,
        allowSorting: false,
        pageSize: 20,
        maxItemsForAutoExpand: 0,
        autoExpandLevel: 0,
        autoExpandAfterUpdate: false,
        renderFooter: function () {
        },
        formatColumn: function (tree, item, fieldName) {
            return item[fieldName];
        },
        formatRow: function () {
        },
        allowSelection: function () {
            return true;
        },
        onCheck: function () {
            return true;
        },
        onReady: function () {
        },
        addEmptyRow: function () {
            var row = $('<tr class="tree-empty"><td colspan="999">No data to display</td></tr>');
            var node = findNodeById();
            node.after(row);
        },
        stickyParentSelector: null
    }, options);
    o.showCheckAll = o.showCheckAll && o.showCheckBoxes;

    var stateId;
    var newState = function () {
        stateId = Math.random();
    };
    var checkState = function (state) {
        return stateId == state;
    }
    self.data("dynamic-tree", this);
    var name = self.data("name");
    var keyName = self.data("key-name") || name;
    var selectionName = self.data("selection-name") || name + "Selection";
    var postArrayName = self.data("post-array");
    var url = self.data("url");
    var columns = self.data("columns");
    var selectedItems = self.data("selection");
    var columnsDefinition = [];
    var treeData = [];
    var nodes = {};
    var table = null;
    var footer;
    var pages;
    var loadingDiv;
    var preselectionDiv;
    var currentPage = 1;
    treeData.push({
        parentId: null,
        isExpanded: true,
        level: 0,
        loaded: false,
        children: [],
        visible: true,
        state: "U"
    });
    newState();

    var isPromise = function (value) {
        return value && value.then && typeof value.then === 'function';
    };

    var updateSettings = function (settings) {
        o = $.extend(o, settings);
    };

    var loadColumns = function () {
        var columnsStr = columns.split('|');
        for (var i = 0; i < columnsStr.length; i++) {
            var namePercentage = columnsStr[i].split(':');
            columnsDefinition.push({
                name: namePercentage[0],
                width: namePercentage[1],
                displayName: namePercentage[2],
                isSortable: namePercentage[3] === "" ? o.allowSorting : namePercentage[3] === "true",
                textAlign: namePercentage[4]
            });
        }
    };

    function findNodeById(nodeId) {
        if (!nodeId) {
            return self.find("tr.tree-head");
        }

        return nodes[nodeId];
    };

    function findData(nodeId) {
        if (!nodeId) nodeId = null;
        for (var i = 0; i < treeData.length; i++) {
            if (treeData[i].nodeId == nodeId)
                return treeData[i];
        }
        return null;
    };

    function deleteData(nodeId) {
        for (var i = 0; i < treeData.length; i++) {
            if (treeData[i].nodeId == nodeId) {
                treeData.splice(i, 1);
            }
        }
    };

    var renderItemSelectionState = function (key, value) {
        var html = "";
        var itemName = keyName;
        var itemValue = selectionName;
        key = key || "";
        if (postArrayName) {
            itemName = postArrayName + "[" + key + "]." + keyName;
            itemValue = postArrayName + "[" + key + "]." + selectionName;
        }
        html += '<input type="hidden" class="ignore-validation" name="' + itemName + '" value="' + key + '" />';
        html += '<input type="hidden" class="ignore-validation tree-check-status" name="' + itemValue + '" value="' + value + '" />';
        return html;
    }

    var renderSelectionState = function () {
        var html = '<div style="display:none" class="tree-pre-selection">';
        if (selectedItems) {
            for (var i = 0; i < selectedItems.length; i++) {
                if (selectedItems[i].TreeSelectionState == "C") {
                    html += '<span data-id="' + selectedItems[i].Key + '">';
                    html += renderItemSelectionState(selectedItems[i].Key, selectedItems[i].TreeSelectionState);
                    html += "</span>";
                }
            }
        }
        html += "</div>";
        return html;
    };

    var resizeDropdownContent = function () {
        var position = self.data("sender-position");
        if (!position) return;

        var wndHeight = self.data("sender-window-height");
        var ddContent = self.parents("div.dropdown-content");

        if (ddContent.length > 0 && wndHeight) {

            var outerHeight = ddContent.outerHeight();
            var resizeByPosition = wndHeight - position.top < 220;
            var resizeBySize = wndHeight < position.top + outerHeight;

            if (resizeByPosition || resizeBySize) {
                var drpdTrigger = ddContent.data('dropdown-trigger');
                var drpdTriggerHeigth = 0;
                if (drpdTrigger) {
                    drpdTriggerHeigth = drpdTrigger.outerHeight();
                }
                var newtop = position.top - outerHeight - drpdTriggerHeigth;
                ddContent.css({ top: newtop + "px" });
            }
        }
    };
    self.on("resize-dropdown-content", function () {
        resizeDropdownContent();
    });

    var addNode = function (parent, data, currentNode) {

        var isParentRoot = !parent.nodeId;
        var isNodeSelected = parent.state == "C" && !o.singleSelection && o.recursiveSelection;
        var nodeState = isNodeSelected ? "C" : "U";

        if (selectedItems) {
            for (var i = 0; i < selectedItems.length; i++) {
                if (selectedItems[i].Key == data.Key) {
                    nodeState = selectedItems[i].TreeSelectionState;
                    if (nodeState == "C") {
                        preselectionDiv.children("[data-id='" + data.Key + "']").remove();
                    }
                    break;
                }
            }

            if (!isParentRoot && parent.state == "U" && o.propagateSelectionToParents) {
                nodeState = "U";
            }
        }

        var item = {
            parentId: parent.nodeId,
            nodeId: data.Key,
            dataItem: data,
            isExpanded: false,
            CanExpand: data.CanExpand,
            state: nodeState,
            level: parent.level + 1,
            loaded: false,
            children: [],
            visible: true
        };

        var rowClass = "tree-data-row row-visible";
        if ((o.showCheckBoxes || o.checkByClick) && (data.CanSelect !== false)) {
            rowClass += ' allow-check-boxes';
        }
        if (data.CanExpand) {
            rowClass += " tree-expandable";
        }
        if (o.allowSelection(self, data, item) && !o.isReadOnly) {
            rowClass += " allow-selection";
        } else {
            rowClass += " deny-selection";
        }

        var checkedState = "";
        if (nodeState == "C") {
            rowClass += " row-selected";
            checkedState = " tree-checked";
        }
        if (nodeState == "I") {
            checkedState = " tree-grayed";
        }

        var rowHtml = '<tr data-id="' + data.Key + '" class="' + rowClass + '">';

        for (var j = 0; j < columnsDefinition.length; j++) {
            var nodeValue = '';

            var contentStyle = '';
            if (j == 0) {
                contentStyle = ' style="padding-left:' + (parent.level) * 20 + 'px"';
                nodeValue += '<span class="tree-expand-area ajax tree-node">';
                var expandableItemTemplate = '<span class="tree-expand ';
                if (!data.CanExpand) {
                    expandableItemTemplate += 'hidden ';
                }
                if (data.CanExpand && currentNode && currentNode.isExpanded) {
                    expandableItemTemplate += 'tree-expanded ';
                }
                nodeValue += expandableItemTemplate + '"></span></span>';

                if ((o.showCheckBoxes || o.checkByClick) && (data.CanSelect !== false)) {
                    var checkClass = "tree-check-area";

                    if (!o.showCheckBoxes) checkClass += " hidden";

                    nodeValue += '<span class="' + checkClass + '">';
                    nodeValue += '<span class="tree-check' + checkedState + '"></span>';
                    nodeValue += '</span>';
                    nodeValue += renderItemSelectionState(data.Key, nodeState);
                }
            }

            var formattedValue = o.formatColumn(self, data, columnsDefinition[j].name);
            if (formattedValue === null)
                formattedValue = "";

            nodeValue += formattedValue;
            rowHtml += '<td class="tree-cell"' + (data.Tooltip ? ' title="' + data.Tooltip + '"' : '') + ' style="min-width:' + columnsDefinition[j].width + '; text-align:' + columnsDefinition[j].textAlign + ';"  data-field="' + columnsDefinition[j].name + '"><div class="tree-content"' + contentStyle + '>' + nodeValue + '</div></td>';
        }

        rowHtml += "</tr>";
        var row = $(rowHtml);
        row.attr('tabIndex', 0);
        o.formatRow(self, {
            data: data,
            row: row
        });
        nodes[data.Key] = row;

        treeData.push(item);
        parent.children.push(item);

        return row;
    };

    var clearTree = function (disableAnimation) {
        newState();
        while (treeData.length > 1) {
            removeById(treeData[1].nodeId, disableAnimation, true);
        }
        renderPages();
        unCheckRecursive();
    };

    var loadingTimer;
    var ajaxCounter = 0;
    var startAjax = function () {
        ajaxCounter++;
        if (ajaxCounter == 1) {
            clearInterval(loadingTimer);
            loadingTimer = setTimeout(function () {
                loadingDiv.removeClass("hidden");
            }, 200);
        }
    };

    var endAjax = function () {
        ajaxCounter--;
        if (!ajaxCounter) {
            clearInterval(loadingTimer);
            loadingDiv.addClass("hidden");
        }
    };

    var loadLevel = function (nodeId, callBack, isUpdate, forceExpandChildren, opts, isPartialUpdate) {
        var i;
        if (!nodeId) nodeId = '';

        var nodeIds = nodeId.toString().split(',');

        var ajaxData = $.extend(opts || {}, {
            parentNodeId: nodeId
        });

        var treeDataAttributes = self.data();
        $.each(treeDataAttributes, function (attrKey, value) {
            if (attrKey.indexOf("ajaxData") == 0) {
                var dataField = attrKey.substring("ajaxData".length);
                ajaxData[dataField] = value;
            }
        });

        if (o.useForm) {
            var form = self.parents("form");
            if (!form.length) {
                alert("Can't find form to send with callback!");
            }
            var formDataArray = form.serializeArray();
            var formData = {};
            for (i = 0; i < formDataArray.length; i++) {
                var formKey = formDataArray[i].name;
                var formValue = formDataArray[i].value;
                if (typeof formData[formKey] == "undefined") {
                    formData[formKey] = formValue;
                } else if ($.isArray(formData[formKey])) {
                    formData[formKey].push(formValue);
                } else {
                    formData[formKey] = [formData[formKey], formValue];
                }
            }
            ajaxData = $.extend(ajaxData, formData);
        }

        if (o.allowSorting) {
            var sortColumns = $("a.tree-column-title", self);
            var requestData = {
                "__SORT_COLUMNS": ""
            };
            var sortData = [];

            var firstColumn = $("th.tree-column").first();
            var isFirstColumnSorted = false;

            for (i = 0; i < sortColumns.length; i++) {
                var sc = $(sortColumns[i]);
                if (sc.hasClass("asc") || sc.hasClass("desc")) {
                    var th = sc.parent();
                    var sortKey = th.data("field") + (sc.hasClass("asc") ? ascendingOrder : descendingOrder);
                    sortData.push(sortKey);
                    if (!isFirstColumnSorted)
                        isFirstColumnSorted = firstColumn.data("field") === th.data("field");
                }
            }

            if (sortData.length > 0) {
                if (!isFirstColumnSorted) {
                    sortData.push(firstColumn.data("field") + ascendingOrder);
                }
                requestData["__SORT_COLUMNS"] = sortData.join("|");
            }

            ajaxData = $.extend(ajaxData, requestData);
        }

        ajaxData.stateId = stateId;

        var pageBeforeUpdate = currentPage;
        startAjax();
        var ajax = $.ajax({
            stateId: stateId,
            url: url,
            data: ajaxData,
            method: "POST",
            success: function (data) {
                var i;

                if (!checkState(this.stateId))
                    return;

                if (!data)
                    return;

                var isFullUpdate = isUpdate && !isPartialUpdate;
                if (isUpdate) {
                    if (isFullUpdate) {
                        clearTree(true);
                    } else {
                        for (i = 0; i < nodeIds.length; i++) {
                            removeChildrenInternal(nodeIds[i]);
                        }
                    }
                    $(self).trigger('dtree-updated', this);
                } else {
                    $(self).trigger('dtree-loaded', this);
                }

                $(".tree-empty", self).remove();

                if (!data.error) {

                    if (!data.length) {
                        if (!nodeId || isUpdate) {
                            var rootData = findData();
                            o.addEmptyRow(self, rootData);
                        }
                        if (callBack) callBack(self, null);
                    }

                    for (var n = 0; n < nodeIds.length; n++) {
                        var parentNodeId = nodeIds[n];
                        var rows = [];

                        var nodeData = findData(parentNodeId);
                        if (!nodeData) {
                            if (!isUpdate) {
                                alert("Can't find parent node with id = " + parentNodeId + " for inserting child nodes.");
                            }
                            continue;
                        }

                        var level = nodeData.level + 1;
                        var node = findNodeById(parentNodeId);
                        var nodeDatas = [];

                        var totalCount = 0;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].ParentKey == parentNodeId) {
                                if (level == 1)
                                    totalCount += data[i].Count;

                                nodeDatas.push(data[i]);
                                var row = addNode(nodeData, data[i]);
                                DynamicTree.globals.initRow(row);
                                rows.push(row);
                            }
                        }
                        if (level == 1)
                            totalItemsCount = totalCount;

                        node.after(rows);
                        resizeDropdownContent();

                        var autoExpandAndLoadChildren = function (level, node, nodeDatas, nodeData, forceExpandChildren) {
                            var autoExpand = nodeData && nodeData.dataItem && nodeData.dataItem.Count <= o.maxItemsForAutoExpand;
                            autoExpand = autoExpand || level <= o.autoExpandLevel || forceExpandChildren;

                            if (autoExpand) {
                                for (var j = 0; j < nodeDatas.length; j++) {
                                    if (nodeDatas[j].CanExpand) {
                                        var nodeToExpand = findNodeById(nodeDatas[j].Key);
                                        expandCollapseLoadRow(nodeToExpand, forceExpandChildren, function () {
                                            if (callBack) callBack(self, node);
                                        });
                                    }
                                }
                            } else {
                                if (callBack) callBack(self, node);
                            }
                        };

                        if (isUpdate) {
                            currentPage = pageBeforeUpdate;
                            expand(parentNodeId, true);
                            nodeData.isExpanded = true;
                            nodeData.loaded = true;
                            node.find(".tree-expand").addClass("tree-expanded");
                        } else {
                            autoExpandAndLoadChildren(level, node, nodeDatas, nodeData, forceExpandChildren);
                        }
                    }
                    if (isUpdate && o.autoExpandAfterUpdate) {
                        if (totalItemsCount && totalItemsCount <= o.maxItemsForAutoExpand) {
                            expandAllVisibleNodes();
                        }
                    }
                    if (callBack) callBack(self);
                    if (o.showCheckAll && isFullUpdate) {
                        checkParents();
                    }
                    o.onReady();
                    self.trigger("dtree-ready");
                }

                renderPages();

                onRendered();
            },
            complete: function () {
                endAjax();
            }
        });
        ajax.fail(function () {
            var attemptedLevelData = findData(nodeId);
            attemptedLevelData.loaded = false;
        });
    };

    function loadContent(callback) {
        loadLevel(null, callback);
    };

    function onRendered() {
        $(self).trigger('rendered');
    };

    var renderTree = function (doLoadContent) {
        var i;
        var html = "";

        html += renderSelectionState();

        html += '<table class="tree">';
        var headAttrClass = "tree-head";
        headAttrClass += o.allowColumnReordering ? " sortable" : "";
        headAttrClass += o.showCheckAll ? " allow-selection" : "";
        var headHtml = '<tr class="' + headAttrClass + '">';

        for (i = 0; i < columnsDefinition.length; i++) {
            var columnTitle = columnsDefinition[i].displayName;
            var check = "";
            if (i == 0 && o.showCheckAll) {
                check += '<span class="tree-check-area">';
                check += '<span class="tree-check"></span>';
                check += '</span>';
            }

            if (o.allowSorting && columnsDefinition[i].displayName.length !== 0 && columnsDefinition[i].isSortable) {
                columnTitle = '<a href="#" tabindex="-1" class="tree-column-title">' + columnTitle + '</a>';
            }

            headHtml += '<th class="tree-column" style="min-width:' + columnsDefinition[i].width + '; text-align:' + columnsDefinition[i].textAlign + ';" data-field="' + columnsDefinition[i].name + '">' +
                check + columnTitle +
                '</th>';
        }
        headHtml += "</tr>";

        var footerHtml = "";
        if (o.pageSize) {
            footerHtml = '<tr class="tree-footer"><td class="pages" colspan="' + columnsDefinition.length + '"></td></tr>';
        }
        html += headHtml + footerHtml + '</table>';

        html += DynamicTree.globals.loaderHtml;

        table = $(html);
        o.renderFooter(self, {
            table: table.addBack().filter("table.tree")
        });
        self.html(table);
        footer = self.find(".tree-footer");
        pages = footer.find(".pages");
        loadingDiv = self.find(".tree-loader");
        preselectionDiv = self.find(".tree-pre-selection");

        var animation = false,
            animationstring = 'animation',
            keyframeprefix = '',
            domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
            pfx = '',
            elm = document.createElement('div');

        if (elm.style.animationName !== undefined) {
            animation = true;
        }

        if (animation === false) {
            for (i = 0; i < domPrefixes.length; i++) {
                if (elm.style[domPrefixes[i] + 'AnimationName'] !== undefined) {
                    pfx = domPrefixes[i];
                    animationstring = pfx + 'Animation';
                    keyframeprefix = '-' + pfx.toLowerCase() + '-';
                    animation = true;
                    break;
                }
            }
        }
        if (!animation) {
            loadingDiv.addClass("not-animated");
        }

        if (doLoadContent)
            loadContent();

        if (o.allowColumnReordering) {
            $(".tree-head.sortable", table).easySortable({
                items: "th:gt(0)"
            }).bind("sortupdate", function (event, dragOptions) {
                var item = columnsDefinition.splice(dragOptions.fromIndex, 1);
                columnsDefinition.splice(dragOptions.toIndex, 0, item[0]);
                refresh();
            });
        }

        if (o.allowSorting) {
            $("a.tree-column-title", table).on("click", function (e) {
                e.preventDefault();

                var columnHeader = $(e.target);
                var nextSortDirection = columnHeader.hasClass("asc") ? "desc" : "asc";
                $("a.tree-column-title", table).removeClass("asc").removeClass("desc");

                columnHeader.addClass(nextSortDirection);

                refresh();
            });
        }
        if (o.stickyParentSelector) {
            initStickyHeader();
        }
    };

    function initStickyHeader() {
        var scrollableArea = self.closest(o.stickyParentSelector);
        var headerCell = self.find(".tree-head th.tree-column");
        var tableHeader = self[0];
        scrollableArea.on('scroll', function () {
            var scrollSize = 0;
            if (scrollableArea[0].getBoundingClientRect().top >= tableHeader.getBoundingClientRect().top) {
                scrollSize = scrollableArea[0].getBoundingClientRect().top - tableHeader.getBoundingClientRect().top;
                headerCell.addClass('tree-column-sticky');
            } else {
                headerCell.removeClass('tree-column-sticky');
            }
            var translate = "translate(0, " + scrollSize + "px)";
            headerCell.css({
                "-webkit-transform": translate,
                "-moz-transform": translate,
                "-o-transform": translate,
                "-ms-transform": translate,
                "transform": translate
            });
        });
    }

    function renderPages() {
        var rowsCount = $("tr.row-visible", self).length;
        var pagesCount = Math.ceil(rowsCount / o.pageSize);
        if (currentPage > pagesCount)
            currentPage = pagesCount;
        if (currentPage == 0)
            currentPage = 1;

        var pagesHtml = '<div><div class="float-left">';

        if (pagesCount > 1) {
            if (currentPage > 1) {
                pagesHtml += '<a class="page page-prev" href="javascript:">Prev</a>';
            } else {
                pagesHtml += '<span class="page page-prev">Prev</span>';
            }

            var currentPageContent = "";
            for (var k = -2; k <= 2; k++) {
                var pn = currentPage + k;
                if (pn < 1 || pn > pagesCount)
                    continue;
                if (k == 0) {
                    currentPageContent += '<span class="page page-current">' + pn + "</span>";
                } else {
                    currentPageContent += '<a class="page" data-page="' + pn + '" href="javascript:">' + pn + "</a>";
                }
            }

            var beforeCurrentPageContent = "";
            for (var l = 1; l < currentPage - 2; l++) {
                if (l == 3) {
                    beforeCurrentPageContent += '<span class="">…</span>';
                    break;
                }
                beforeCurrentPageContent += '<a class="page" data-page="' + l + '" href="javascript:">' + l + "</a>";
            }

            var afterCurrentPageContent = "";
            for (var m = pagesCount, counter = 1; m > currentPage + 2; m--, counter++) {
                if (counter == 3) {
                    afterCurrentPageContent = '<span class="">…</span>' + afterCurrentPageContent;
                    break;
                }
                afterCurrentPageContent = '<a class="page" data-page="' + m + '" href="javascript:">' + m + "</a>" + afterCurrentPageContent;
            }

            pagesHtml += beforeCurrentPageContent + currentPageContent + afterCurrentPageContent;

            if (currentPage < pagesCount) {
                pagesHtml += '<a class="page page-next" href="javascript:">Next</a>';
            } else {
                pagesHtml += '<span class="page page-next">Next</span>';
            }

        }

        pagesHtml += '</div>';
        if (!o.hideTotalCount) {
            pagesHtml += '<div class="tree-page-count"><span>Total Count: ' + totalItemsCount + ' </span></div></div>';
        }

        pages[0].innerHTML = pagesHtml;
        switchToPage();
    };

    function switchToPage() {
        var rows = $("tr", self);

        var index = 0;
        var startIndex = o.pageSize * (currentPage - 1);
        var endIndex = startIndex + o.pageSize;
        rows.each(function () {
            var row = $(this);
            if (row.hasClass("row-visible")) {
                if (startIndex <= index && index < endIndex) {
                    row.removeClass("hidden");
                } else {
                    row.addClass("hidden");
                }
                index++;
            }
        });

        self.trigger("tree-page-changed");
    };

    var getItemPage = function (key) {
        var result = 0;
        var rows = $("tr", self);

        var index = 0;
        rows.each(function () {
            var row = $(this);
            if (row.hasClass("row-visible")) {

                var id = row.data("id");
                if (key == id) {
                    result = Math.floor(index / o.pageSize) + 1;
                }

                index++;
            }
        });

        return result;
    };

    var refreshInternal = function (nodeIds, opts, callback) {
        newState();

        if (nodeIds && !nodeIds.length) {
            nodeIds = null;
        }
        if (nodeIds && typeof (nodeIds) == "string") {
            nodeIds = [nodeIds];
        }
        var i;

        if (o.showCheckBoxes) {
            var selection = getSelection(true);
            selectedItems = [];
            for (i = 0; i < selection.length; i++) {
                var item = selection[i];
                if (item.state != "U") {
                    selectedItems.push({
                        Key: item.nodeId,
                        TreeSelectionState: item.state
                    });
                }
            }
        }

        var nodesToLoad = [];
        if (!nodeIds) {
            nodesToLoad.push('');

            $("tr.tree-data-row", self).each(function () {
                var id = $(this).data("id");
                var data = findData(id);
                if (id && data.isExpanded) {
                    nodesToLoad.push(id);
                }
            });
        } else {
            var descendantIds = nodeIds;
            while (descendantIds.length) {
                var currentNodeId = descendantIds.splice(0, 1)[0];
                if (nodesToLoad.indexOf(currentNodeId) == -1) {
                    nodesToLoad.push(currentNodeId);
                } else {
                    continue;
                }
                var data = findData(currentNodeId);
                if (data) {
                    for (i = data.children.length - 1; i >= 0; i--) {
                        var childId = data.children[i].nodeId;
                        var childData = findData(childId);
                        if (childData && childData.isExpanded) {
                            descendantIds.push(childId);
                            if (nodesToLoad.indexOf(childId) == -1) {
                                nodesToLoad.push(childId);
                            }
                        }
                    }
                }
            }
        }

        var nodeIdToLoad = nodesToLoad.join();
        loadLevel(nodeIdToLoad, callback, true, false, opts, !!nodeIds);
    }

    var refreshNodes = function (nodeIds, opts, callback) {
        refreshInternal(nodeIds, opts, callback);
    };

    function refresh(opts, callback) {
        refreshInternal(null, opts, callback);
    };

    var expandNodes = function (nodeKeys, callback) {
        if (nodeKeys && nodeKeys.length && nodeKeys[0]) {
            nodeKeys.splice(0, 0, "");
        }
        newState();
        var nodeIds = nodeKeys.join();
        loadLevel(nodeIds, callback, true);
    };

    var navigateAndSelect = function (key) {
        doCheckNode(key);
        var keyPage = getItemPage(key);
        if (keyPage) {
            currentPage = keyPage;
            switchToPage();
        }
    }

    var hideChildren = function (nodeId, disableUi) {
        var nodeData = findData(nodeId);

        for (var i = 0; i < nodeData.children.length; i++) {
            var id = nodeData.children[i].nodeId;

            var childNode = findNodeById(id);

            childNode.removeClass("row-visible");
            hideChildren(id, disableUi);
        }
        resizeDropdownContent();

        if (!disableUi) renderPages();
    };

    var collapse = function (nodeId, disableUi) {
        var nodeData = findData(nodeId);
        nodeData.isExpanded = false;

        hideChildren(nodeId, disableUi);
    };

    var showChildren = function (nodeId, disableUi, forceExpandChildren) {
        var nodeData = findData(nodeId);

        if (forceExpandChildren && !nodeData.loaded && nodeData.CanExpand) {
            nodeData.loaded = true;
            nodeData.isExpanded = true;
            loadLevel(nodeId, null, false, forceExpandChildren);
            return;
        }

        for (var i = 0; i < nodeData.children.length; i++) {
            var id = nodeData.children[i].nodeId;

            var childNode = findNodeById(id);
            var childNodeData = findData(id);

            childNode.addClass("row-visible");

            if (childNodeData.isExpanded || forceExpandChildren) {
                var expandButton = childNode.find(".tree-expand");
                expandButton.addClass("tree-expanded");
                showChildren(id, disableUi, forceExpandChildren);
            }
        }

        resizeDropdownContent();

        if (!disableUi) renderPages();
    };

    function expand(nodeId, disableUi, forceExpandChildren) {
        var node = findNodeById(nodeId);
        var expandButton = node.find(".tree-expand");
        expandButton.addClass("tree-expanded");

        var nodeData = findData(nodeId);
        nodeData.isExpanded = true;

        showChildren(nodeId, disableUi, forceExpandChildren);
    };

    function checkRecursive(nodeId) {
        if (o.singleSelection) {
            clearSelection();
        }

        var node = findNodeById(nodeId);
        var nodeData = findData(nodeId);
        if (!node || !nodeData)
            throw new Error("Item " + nodeId + " is not found in a tree!");
        nodeData.state = "C";
        node.find("input.tree-check-status").val(nodeData.state);
        node.find(".tree-check").attr("class", "tree-check tree-checked");
        if (nodeData.level) {
            node.addClass("row-selected");
        }

        if (!o.singleSelection && o.recursiveSelection) {
            for (var i = 0; i < nodeData.children.length; i++) {
                var id = nodeData.children[i].nodeId;
                checkRecursive(id);
            }
        }
    };

    function unCheck(node, nodeData) {
        nodeData.state = "U";
        node.removeClass("row-selected");
        node.find("input.tree-check-status").val(nodeData.state);
        node.find(".tree-check").attr("class", "tree-check");
    }

    function unCheckRecursive(nodeId) {
        var node = findNodeById(nodeId);
        var nodeData = findData(nodeId);
        unCheck(node, nodeData);

        if (!o.singleSelection && o.recursiveSelection) {
            for (var i = 0; i < nodeData.children.length; i++) {
                var id = nodeData.children[i].nodeId;
                unCheckRecursive(id);
            }
        }
    };

    function checkParents(nodeId) {
        if (o.singleSelection)
            return;

        var node = findNodeById(nodeId);
        var nodeData = findData(nodeId);

        var hasChecks = false;
        var allChecked = true;
        for (var i = 0; i < nodeData.children.length; i++) {
            if (nodeData.children[i].state == "C") {
                hasChecks = true;
            }
            if (nodeData.children[i].state == "U") {
                allChecked = false;
            }
            if (nodeData.children[i].state == "I") {
                hasChecks = true;
                allChecked = false;
            }
        }

        var state = "U";
        if (hasChecks) {
            if (o.propagateSelectionToParents) {
                state = "I";
            } else {
                if (nodeData.state == "C") {
                    state = "C";
                }
            }
            if (allChecked && o.propagateSelectionToParents) {
                state = "C";
            }
        }
        nodeData.state = state;

        node.find("input.tree-check-status").val(nodeData.state);
        if (state == "U") {
            node.removeClass("row-selected").find(".tree-check").attr("class", "tree-check");
        }
        if (state == "I") {
            node.removeClass("row-selected").find(".tree-check").attr("class", "tree-check tree-grayed");
        }
        if (state == "C") {
            node.addClass("row-selected").find(".tree-check").attr("class", "tree-check tree-checked");
        }

        if (nodeData.level) {
            checkParents(nodeData.parentId);
        }
    };

    function doCheckNode(nodeId) {
        checkRecursive(nodeId);
        var nodeData = findData(nodeId);
        checkParents(nodeData.parentId);
        self.triggerHandler("dtree-checked", nodeId);
    };

    function doUncheckNode(nodeId) {
        unCheckRecursive(nodeId);
        var nodeData = findData(nodeId);
        if (!o.singleSelection && o.propagateSelectionToParents) {
            checkParents(nodeData.parentId);
        }
        self.triggerHandler("dtree-unchecked", nodeId);
    };

    function expandCollapseLoadRow(row, forceExpandChildren, callBack) {
        var nodeId = row.data("id");
        var data = findData(nodeId);

        var notifyExpanded = function () {
            self.trigger("tree-node-expanded");
        };
        var notifyCollapsed = function () {
            self.trigger("tree-node-collapsed");
        };

        var expandButton = row.find(".tree-expand");
        if (data && data.CanExpand) {
            if (!data.loaded) {
                expandButton.addClass("tree-expanded");
                data.loaded = true;
                data.isExpanded = true;
                loadLevel(nodeId, function (self, node) {
                    if (callBack) callBack(self, node);
                    notifyExpanded(self, node);
                }, false, forceExpandChildren);
            } else {
                if (data.isExpanded) {
                    expandButton.removeClass("tree-expanded");
                    collapse(nodeId, true);
                    notifyCollapsed(self, row);
                } else {
                    expandButton.addClass("tree-expanded");
                    expand(nodeId, true, forceExpandChildren);
                    notifyExpanded(self, row);
                }
                if (callBack) callBack(self, row);
            }
        }
        renderPages();
    };

    function collapseAll() {
        for (var i = 0; i < treeData.length; i++) {
            if (treeData[i].level > 0 && treeData[i].isExpanded) {
                var nodeId = treeData[i].nodeId;
                var data = findData(nodeId);
                var row = findNodeById(nodeId);
                var expandButton = row.find(".tree-expand");

                if (data.isExpanded) {
                    expandButton.removeClass("tree-expanded");
                    collapse(nodeId, true);
                }

                collapse(nodeId, true);
            }
        }
        renderPages();
    };

    function expandAllVisibleNodes() {
        for (var i = 0; i < treeData.length; i++) {
            if (treeData[i].level > 0 && !treeData[i].isExpanded) {
                var nodeId = treeData[i].nodeId;
                var row = findNodeById(nodeId);

                expandCollapseLoadRow(row, null);
            }
        }
        renderPages();
    };

    function removeChildrenInternal(nodeId) {
        var data = findData(nodeId);
        if (!data) return;
        for (var i = data.children.length - 1; i >= 0; i--) {
            var childId = data.children[i].nodeId;
            removeChildrenInternal(childId);
            removeByIdInternal(childId, true, true);
        }
    }

    function removeByIdInternal(nodeId, disableAnimation, disableUi) {
        var data = findData(nodeId);
        deleteData(nodeId);
        var row = findNodeById(nodeId);
        if (disableAnimation) {
            if (row[0].parentNode) {
                row[0].parentNode.removeChild(row[0]);
            }
            if (!disableUi) renderPages();
        } else {
            row.find("div.tree-content, td, tr").addBack().animate({ padding: 0, height: 0, opacity: 0 }, 500, function () {
                $(this).remove();
                if (!disableUi) renderPages();
            });
        }
        return data;
    }

    function removeById(nodeId, disableAnimation, disableUi, recalculateParents) {
        var i;

        var removedNodeId = nodeId;
        var removeNodeData = findData(removedNodeId);
        var parentData;

        while (nodeId) {
            var data = removeByIdInternal(nodeId, disableAnimation, disableUi);

            var parentNodeId = null;
            parentData = findData(data.parentId);
            if (parentData) {
                for (i = parentData.children.length - 1; i >= 0; i--) {
                    if (parentData.children[i].nodeId == nodeId) {
                        parentData.children.splice(i, 1);
                    }
                }

                if (!parentData.children.length) {
                    if (parentData.dataItem && parentData.dataItem.KeepWithoutValuedChildren === true) {
                        break;
                    }
                    parentNodeId = data.parentId;
                }
            }

            for (i = data.children.length - 1; i >= 0; i--) {
                removeChildrenInternal(data.children[i].nodeId);
            }

            nodeId = parentNodeId;
        }

        if (recalculateParents && removeNodeData) {

            var parentNodeData = findData(removeNodeData.parentId);
            parentNodeData.isExpanded = !!parentNodeData.children.length;
            if (!parentNodeData.children.length) {
                parentNodeData.CanExpand = false;
                parentNodeData.dataItem.CanExpand = false;
            }

            refreshNodeWithParents(parentNodeData.nodeId, function (node) {
                node.dataItem.Count -= (removeNodeData.dataItem.NodeValue || removeNodeData.dataItem.Count);
            });
        }
    };

    function refreshNodeWithParents(nodeId, nodeCallBack) {
        var node = findData(nodeId);

        while (node && node.dataItem) {
            nodeCallBack(node);

            refreshNodeData(node.nodeId, node.dataItem);

            node = findData(node.parentId);
        }
    }

    function refreshNodeData(nodeId, data) {
        var node = findData(nodeId);

        var parent = findData(node.parentId);
        if (parent) {
            var currentNode = findNodeById(node.nodeId);
            var newNode = addNode(parent, data, node);
            currentNode.replaceWith(newNode);
            DynamicTree.globals.initRow(newNode);
        }
    }

    function getSelection(includeIndeterminate) {
        var selectedRows = includeIndeterminate ? $("tr.allow-selection", self) : $(".tree-checked", self).parents("tr");

        var selectionData = [];
        var seelctedIds = [];

        selectedRows.each(function () {
            var id = $(this).data("id");
            var data = findData(id);
            if (data && (!(data.loaded && data.CanExpand) && !includeIndeterminate) || includeIndeterminate) {
                seelctedIds.push(id);
                selectionData.push(data);
            }
        });

        return selectionData;
    };

    function clearSelection() {
        var selectedRows = $(".tree-checked,.tree-grayed", self).parents("tr");

        selectedRows.each(function () {
            var nodeId = $(this).data("id");

            var node = findNodeById(nodeId);
            var nodeData = findData(nodeId);

            unCheck(node, nodeData);
        });
    }

    $(self).on("click", "a.page", function () {
        var page = $(this).data("page");
        if (page) {
            currentPage = page;
            renderPages();
        }
    });

    $(self).on("click", "a.page-prev", function () {
        currentPage--;
        renderPages();
    });

    $(self).on("click", "a.page-next", function () {
        currentPage++;
        renderPages();
    });

    $(self).on("click", ".tree-expand", function (e) {
        e.stopPropagation();
        var row = $(this).parents("tr");
        expandCollapseLoadRow(row);
    });

    $(self).on("click", "span.tree-check", function (e) {
        e.stopPropagation();
        if (o.checkByClick || o.showCheckBoxes) {
            const row = $(this).parents("tr");
            if (row.hasClass("allow-selection")) {
                const nodeId = row.data("id");
                const data = findData(nodeId);
                if (data.state === "U") {
                    const checkResult = o.onCheck($(this), data, "check");
                    if (typeof (checkResult) === 'boolean' && checkResult) {
                        doCheckNode(nodeId);
                    }
                    if (isPromise(checkResult)) {
                        startAjax();
                        checkResult.then(function (result) {
                            if (result) {
                                doCheckNode(nodeId);
                            }
                            endAjax();
                        }, endAjax);
                    }
                } else if (data.state === "C" || data.state === "I") {
                    doUncheckNode(nodeId);
                }
            }
        }

        self.triggerHandler("dtree-selection-change");
    });

    $(self).on("click", "tr", function () {
        var row = $(this);
        var check = row.find("span.tree-check");
        if (o.checkByClick)
            check.click();
    });
    $(self).on("keydown ", "tr.tree-data-row", function (e) {
        if ($.inArray(e.keyCode, [vkEnter, vkSpace, vkLeft, vkUp, vkRight, vkDown]) != -1)
            e.preventDefault();
    });
    $(self).on("keyup", "tr.tree-data-row", function (e) {
        var row = $(this);
        var nextRow = row.nextAll(".tree-data-row.row-visible:first");
        var prevRow = row.prevAll(".tree-data-row.row-visible:first");
        var rowExpandIcon = $(".tree-expand", row);
        var rowCheckbox = $("span.tree-check", row);
        var rowLink = $("a[href]:first", row);
        switch (e.keyCode) {
            case vkEnter:
                if (rowLink.length)
                    rowLink.click();
                break;
            case vkSpace:
                if (rowCheckbox.length)
                    rowCheckbox.click();
                break;
            case vkLeft:
                if (rowExpandIcon.hasClass("tree-expanded"))
                    rowExpandIcon.click();
                break;
            case vkUp:
                if (prevRow.length)
                    prevRow.focus();
                break;
            case vkRight:
                if (!rowExpandIcon.hasClass("tree-expanded"))
                    rowExpandIcon.click();
                break;
            case vkDown:
                if (nextRow.length)
                    nextRow.focus();
                break;
        }
        e.preventDefault();
        e.stopPropagation();
    });

    var acivateMenu = function () {
        var menu = $('<div class="cctx"/>');
        var menuOptions = [];
        menuOptions.push({
            title: "Expand Branch",
            onclick: function (key) {
                var row = findNodeById(key);
                if (row) {
                    expandCollapseLoadRow(row, true);
                }
            }
        });

        $.each(menuOptions, function () {
            var item = this;
            var el = $('<div/>');

            if (item.separator) {
                el.addClass('cctx-separator');
            } else {
                el.addClass('cctx-item');
                el.text(item.title);
                el.on('click', function () {
                    var key = $(this).parents(".cctx").data("key");
                    item.onclick(key);
                });
            }

            menu.append(el);
        });

        $('body').append(menu);

        $(self).on("contextmenu", "tr", function (e) {
            var row = $(this);
            var nodeId = row.data("id");
            if (nodeId) {
                var data = findData(nodeId);
                if (data.CanExpand && !data.isExpanded) {
                    e.preventDefault();
                    menu.data("key", nodeId);
                    menu.css('top', e.clientY).css('left', e.clientX).show();
                }
            }
        });

        $(self).parents().on('mouseup', function () {
            $('.cctx:visible').hide();
        });

        $(self).on("remove", function () {
            menu.remove();
        });
    };

    if (o.showContextMenu) {
        acivateMenu();
    }

    loadColumns();

    if (o.autoRender) {
        renderTree(true);
    }

    this.renderTree = renderTree;
    this.loadContent = loadContent;
    this.collapseAll = collapseAll;
    this.findData = findData;
    this.addNode = function (parent, item, recalculateParents) {
        if (typeof parent === "string") {
            parent = findData(parent);
        }
        var lastData = parent;
        while (lastData.children && lastData.children.length > 0) {
            lastData = lastData.children[lastData.children.length - 1];
        }

        var lastChild = findNodeById(lastData.nodeId);

        parent.isExpanded = true;
        parent.CanExpand = true;
        if (parent.dataItem) {
            parent.dataItem.CanExpand = true;
        }

        var row = addNode(parent, item);
        lastChild.after(row);
        DynamicTree.globals.initRow(row);
        if (recalculateParents) {
            refreshNodeWithParents(parent.nodeId, function (node) {
                node.dataItem.Count += (item.NodeValue || item.Count);
            });
        }
    };

    this.expand = function (nodeId, callBack) {
        var row = findNodeById(nodeId);
        var data = findData(nodeId);
        if (!data.isExpanded) {
            expandCollapseLoadRow(row, callBack);
        } else {
            if (callBack) callBack(self, row);
        }
    };

    this.clearSelection = clearSelection;
    this.findNodeById = findNodeById;
    this.removeById = removeById;
    this.getSelection = getSelection;
    this.refresh = refresh;
    this.refreshNodeData = refreshNodeData;
    this.refreshNodes = refreshNodes;
    this.getData = function () {
        return treeData;
    };
    this.selectAll = function () {
        $.each(treeData, function (index, node) {
            if (!node.parentId && node.nodeId) {
                doCheckNode(node.nodeId);
            }
        });
    };
    this.getElement = function () {
        return self;
    }

    this.expandNodes = expandNodes;
    this.navigateAndSelect = navigateAndSelect;
    this.updateSettings = updateSettings;

    this.unCheckNode = function (key) {
        if (o.propagateSelectionToParents) throw new Error("Is not supported");
        var hasSelectionData = false;

        //<VN> workaround to support MultiOrgUnitTreePicker in InlineTableEditor
        if (selectedItems.constructor.toString().indexOf("Array") == -1) {
            var items = selectedItems.split(',');
            selectedItems = [];
            for (var i = 0; i < items.length; i++) {
                selectedItems.push({
                    Key: items[i],
                    TreeSelectionState: 'C'
                });
            }
        }

        for (var i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].Key == key) {
                selectedItems[i].TreeSelectionState = 'U';
                hasSelectionData = true;
                break;
            }
        }

        if (!hasSelectionData) {
            selectedItems.push({
                Key: key,
                TreeSelectionState: 'U'
            });
        }
        var nodeData = findData(key);
        if (nodeData) {
            doUncheckNode(key);
        }
    };
    this.checkNode = function (key) {
        if (o.propagateSelectionToParents) throw new Error("Is not supported");
        var hasSelectionData = false;
        for (var i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].Key == key) {
                selectedItems[i].TreeSelectionState = 'C';
                hasSelectionData = true;
                break;
            }
        }

        if (!hasSelectionData) {
            selectedItems.push({
                Key: key,
                TreeSelectionState: 'C'
            });
        }
        var nodeData = findData(key);
        if (nodeData) {
            doCheckNode(key);
        }
    };

    this.updateUrl = function (newUrl) {
        url = newUrl;
        if (typeof loadingDiv !== "undefined") {
            clearTree(true);
            refreshInternal();
        }
    };
};

DynamicTree.globals = {
    initRow: function () { },
    loaderHtml: '<div class="loading-message tree-loader"></div>'
};