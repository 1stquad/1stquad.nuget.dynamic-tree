﻿/*
 * HTML5 Sortable for Dynamic Tree
 * 
 * Copyright 2015, Dmitry Veresov
 * Released under the MIT license.
 */
(function ($) {
    var dragging, placeholders = $();
    $.fn.easySortable = function (options) {
        var method = String(options);
        options = $.extend({
            connectWith: false
        }, options);
        return this.each(function () {
            if (/^enable|disable|destroy$/.test(method)) {
                items = $(this).children($(this).data('items')).attr('draggable', method == 'enable');
                if (method == 'destroy') {
                    items.add(this).removeData('connectWith items')
                        .off('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
                }
                return;
            }
            var isHandle, index, items = $(this).children(options.items);
            var placeholder = $('<div class="sortable-placeholder"><div class="arrow-down"></div><div class="down-line"></div><div class="arrow-up"></div></div>');
            items.find(options.handle).mousedown(function () {
                isHandle = true;
            }).mouseup(function () {
                isHandle = false;
            });
            $(this).data('items', options.items);
            placeholders = placeholders.add(placeholder);
            if (options.connectWith) {
                $(options.connectWith).add(this).data('connectWith', options.connectWith);
            }
            items.attr('draggable', 'true').on('dragstart.h5s', function (e) {
                if (options.handle && !isHandle) {
                    return false;
                }
                isHandle = false;
                var dt = e.originalEvent.dataTransfer;
                dt.effectAllowed = 'move';
                dt.setData('Text', 'dummy');
                index = (dragging = $(this)).addClass('sortable-dragging').index();
                return true;
            }).on('dragend.h5s', function () {
                dragging.removeClass('sortable-dragging').show();
                placeholders.filter('.sortable-placeholder').detach();
                if (index != dragging.index()) {
                    items.parent().trigger('sortupdate', {
                        fromIndex: index,
                        toIndex: dragging.index(),
                        item: dragging
                    });
                }
                dragging = null;
            }).not('a[href], img').on('selectstart.h5s', function () {
                this.dragDrop && this.dragDrop();
                return false;
            }).end().add([this, placeholder]).on('dragover.h5s dragenter.h5s drop.h5s', function (e) {
                if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
                    return true;
                }
                if (e.type == 'drop') {
                    e.stopPropagation();
                    var ph = placeholders.filter('.sortable-placeholder:visible');
                    var element = ph.data("element");
                    if (!element) return false;
                    element[ph.data("element-pos")](dragging);
                    return false;
                }
                e.preventDefault();
                e.originalEvent.dataTransfer.dropEffect = 'move';
                if (items.is(this)) {
                    if (options.forcePlaceholderSize) {
                        placeholder.height(dragging.outerHeight());
                    }

                    var y = e.originalEvent.pageX - $(this).offset().left;
                    var rightHalf = y > $(this).width() * 0.7;
                    var leftHalf = y < $(this).width() * 0.3 && y > 0;

                    if (rightHalf || leftHalf) {
                        placeholder.prependTo($("body"));
                        placeholder.addClass("absolute-placeholder");
                        placeholder.css("height", $(this).outerHeight());
                        placeholder.data("element", $(this));
                        if (rightHalf) {
                            placeholder.data("element-pos", 'after');
                            placeholder.css("top", $(this).offset().top);
                            placeholder.css("left", $(this).offset().left + $(this).outerWidth());
                        }
                        if (leftHalf) {
                            placeholder.data("element-pos", 'before');
                            placeholder.css("top", $(this).offset().top);
                            placeholder.css("left", $(this).offset().left);
                        }
                    }

                    placeholders.filter('.sortable-placeholder').not(placeholder).detach();
                } else if (!placeholders.filter('.sortable-placeholder').is(this) && !$(this).children(options.items).length) {
                    placeholders.filter('.sortable-placeholder').detach();
                    $(this).append(placeholder);
                }
                return false;
            });
        });
    };
})(jQuery);