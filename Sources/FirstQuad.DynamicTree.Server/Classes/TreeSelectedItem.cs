﻿using System;

namespace FirstQuad.DynamicTree.Server.Classes
{
    [Serializable]
    public class TreeSelectedItem
    {
        public TreeSelectedItem()
        {
            
        }

        public TreeSelectedItem(string key, string selectionState)
        {
            Key = key;
            TreeSelectionState = selectionState;
        }

        public string Key { get; set; }

        public string ParentKey { get; set; }

        public int ChildrenCount { get; set; }

        public string TreeSelectionState { get; set; }
    }
}