﻿namespace FirstQuad.DynamicTree.Server.Classes
{
    public enum DecimalFieldFormat
    {
        NoDigits = 0,
        TwoDigits = 1,
        KiloNoDigits = 2,
        KiloTwoDigits = 3,
        MillionNoDigits = 4,
        MillionTwoDigits = 5,
    }
}