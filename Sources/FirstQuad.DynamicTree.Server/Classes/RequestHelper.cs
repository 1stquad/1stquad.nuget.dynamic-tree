﻿#if DOTNET_CORE
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System.Linq;
#else
using System.Web;
#endif

namespace FirstQuad.DynamicTreeCore.Server.Classes
{
    public static class RequestHelper
    {
#if DOTNET_CORE
        public static string GetData(this HttpRequest request, string key)
        {
            if (request.Query.TryGetValue(key, out StringValues qValue))
            {
                return qValue.FirstOrDefault();
            }
            else if (request.Cookies.TryGetValue(key, out string cValue))
            {
                return cValue;
            }
            else if (request.Form.TryGetValue(key, out StringValues fValue))
            {
                return fValue.FirstOrDefault();
            }

            return null;
        }
#else
        public static string GetData(this HttpRequest request, string key)
        {
            return request[key];
        }
#endif
    }
}
