namespace FirstQuad.DynamicTree.Server.Classes
{
    public enum ColumnTextAlign
    {
        Left,
        Right,
        Center
    }
}