﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using FirstQuad.Common.Exceptions;
using FirstQuad.Common.Helpers;
using FirstQuad.DynamicTree.Server.Attributes;
using FirstQuad.DynamicTree.Server.Interfaces;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using FirstQuad.DynamicTreeCore.Server.Classes;
using System.Drawing;
using FirstQuad.DynamicTree.Server.Attributes;
using System.Web;
#if DOTNET_CORE
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FirstQuad.Common.Classes;
using Newtonsoft.Json;
#else
using System.Web;
using System.Web.Script.Serialization;
using IActionResult = System.Web.Mvc.ActionResult;
#endif

namespace FirstQuad.DynamicTree.Server.Classes
{
    public class DynamicTree<TModel> : IDynamicTree<TModel> where TModel : class
    {
        public const string SelectedStateValue = "C";
        public const string UndeterminedStateValue = "I";
        public const string UncheckedStateValue = "U";
        public const string SortColumns = "__SORT_COLUMNS";

        private class TreeKeyItem
        {
            public TreeKeyItem()
            {
                Children = new List<TreeKeyItem>();
                TreeSelectionState = UncheckedStateValue;
            }

            public string Key { get; set; }

            public string ParentKey { get; set; }

            public List<TreeKeyItem> Children { get; set; }

            public string TreeSelectionState { get; set; }
        }

        public class DynamicTreeExecutionContext : IDynamicTreeExecutionContext
        {
            public DynamicTreeExecutionContext()
            {
                ItemKeysToLoad = new List<string>();
            }

            public bool IsDataExtraction { get; set; }
            public bool IsDataExtractionNodeFullySelected { get; set; }
            public List<string> ItemKeysToLoad { get; }
        }

        public static string GetFieldName<T, T1>(Expression<Func<T, T1>> fieldExpression)
        {
            if (ReferenceEquals(fieldExpression, null))
                throw new ArgumentNullException(nameof(fieldExpression));

            var memberExpression = (fieldExpression.Body as MemberExpression);
            if (ReferenceEquals(memberExpression, null))
            {
                var unaryExpression = (fieldExpression.Body as UnaryExpression);
                if (ReferenceEquals(unaryExpression, null))
                    throw new InvalidProgramException("fieldExpression is invalid expression");

                memberExpression = unaryExpression.Operand as MemberExpression;
                if (ReferenceEquals(memberExpression, null))
                    throw new InvalidProgramException("fieldExpression is invalid expression");
            }

            return memberExpression.Member.Name;
        }

        public static string GetPropertyDisplayName<T, T1>(Expression<Func<T, T1>> fieldExpression)
        {
            var propertyName = GetFieldName(fieldExpression);

            var propertyInfo = typeof(T).GetProperty(propertyName);
            if (propertyInfo == null)
                throw new InvalidProgramException($"Can't find property {propertyName} in type {typeof(T).FullName}");

            var displayNameAttr = propertyInfo.GetCustomAttribute<DisplayNameAttribute>();
            return displayNameAttr == null ? propertyName : displayNameAttr.DisplayName;
        }

        private readonly Dictionary<PropertyInfo, Type> _enumColumns = new Dictionary<PropertyInfo, Type>();
        private readonly Dictionary<object, string> _parentKeyMapping = new Dictionary<object, string>();
        private readonly Dictionary<string, Func<TreeItemModel<TModel>, object>> _dynamicColumns = new Dictionary<string, Func<TreeItemModel<TModel>, object>>();
        private readonly List<DynamicTreeColumnBase<TModel>> _columns = new List<DynamicTreeColumnBase<TModel>>();
        private readonly DynamicTreeExecutionContext _executionContext = new DynamicTreeExecutionContext();

        public readonly Dictionary<PropertyInfo, Func<TModel, object, object>> RenderColumns = new Dictionary<PropertyInfo, Func<TModel, object, object>>();

        public IDynamicTreeExecutionContext ExecutionContext => _executionContext;

        public string Name { get; set; }
        public string Action { get; set; }
        public string ClientJs { get; set; }
        public string FormatRowJs { get; set; }
        public string OnCheckJs { get; set; }
        public string FormatColumnJs { get; set; }
        public string RenderFooterJs { get; set; }
        public bool ShowCheckAllCheckBox { get; set; }
        public bool ShowCheckBoxes { get; set; }
        public bool SingleSelection { get; set; }
        public bool RowSelection { get; set; }
        public bool UseForm { get; set; }
        public bool AllowColumnReordering { get; set; }
        public bool AllowSorting { get; set; }
        public bool AutoRenderClientHtml { get; set; }
        public bool AutoExpandAfterUpdate { get; set; }
        public bool HideTotalCount { get; set; }
        public bool ShowContextMenu { get; set; }
        public bool StickyScroll { get; set; }
        public bool IsReadOnly { get; set; }

        public int PageSize { get; set; }
        public int MaxItemsToAutoExpand { get; set; }
        public int AutoExpandLevel { get; set; }
        public bool RecursiveSelection { get; set; }
        public bool PropagateSelectionToParents { get; set; }

        public TreeRenderMode RenderMode { get; set; }

        private readonly HttpContext _httpContext;

        public DynamicTree(HttpContext httpContext) : this()
        {
            _httpContext = httpContext;
        }

#if DOTNET_FULL
        public DynamicTree() : this();
        {
            _httpContext = HttpContext.Current;
        }
#endif

        private DynamicTree()
        {
            ShowCheckAllCheckBox = false;
            ShowCheckBoxes = false;
            SingleSelection = false;
            RowSelection = false;
            AllowColumnReordering = false;
            AllowSorting = false;
            AutoRenderClientHtml = true;
            AutoExpandAfterUpdate = false;
            HideTotalCount = false;
            ShowContextMenu = true;
            PageSize = 100;
            MaxItemsToAutoExpand = 0;
            RenderMode = TreeRenderMode.Html;
            StickyScroll = false;
            RecursiveSelection = true;
            PropagateSelectionToParents = true;
        }

        public List<DynamicTreeColumnBase<TModel>> Columns => _columns;

        public string ColumnsAsString
        {
            get
            {
                var columns = new List<string>();
                foreach (var column in _columns)
                {
                    var sortOptions = (column.SortOptions == ColumnSortOptions.Default) ? string.Empty : (column.SortOptions == ColumnSortOptions.Yes ? "true" : "false");
                    var textAlign = column.TextAlign.ToString();
                    columns.Add($"{column.FieldName}:{column.Size}:{column.DisplayName}:{sortOptions}:{textAlign}");
                }

                return string.Join("|", columns);
            }
        }

        public DynamicTreeColumn<TModel, TValue> AddColumn<TValue>(Expression<Func<TModel, TValue>> field, int size, Action<DynamicTreeColumn<TModel, TValue>> configuration)
        {
            var column = AddColumn(field, size);
            configuration(column);
            return column;
        }

        public DynamicTreeColumn<TModel, TValue> AddColumn<TValue>(Expression<Func<TModel, TValue>> field, int size, ColumnSortOptions sortOptions = ColumnSortOptions.Default, ColumnTextAlign textAlign = ColumnTextAlign.Left)
        {
            var displayName = GetPropertyDisplayName(field);
            var columnTitle = displayName ?? string.Empty;

            return AddColumn(field, columnTitle, size + "px", sortOptions, textAlign);
        }

        public DynamicTreeColumn<TModel, object> AddEnumColumn<TValue>(Expression<Func<TModel, object>> field, int size, ColumnSortOptions sortOptions = ColumnSortOptions.Default) where TValue : struct
        {
            var displayName = GetPropertyDisplayName(field);
            var columnTitle = displayName ?? string.Empty;
            var column = AddColumn(field, columnTitle, size + "px", sortOptions);
            EnumRender<TValue>(field);
            return column;
        }

        public DynamicTreeColumn<TModel, TValue> AddColumn<TValue>(Expression<Func<TModel, TValue>> field, string columnTitle, string size, ColumnSortOptions sortOptions = ColumnSortOptions.Default, ColumnTextAlign textAlign = ColumnTextAlign.Left)
        {
            var columnOptions = new DynamicTreeColumn<TModel, TValue>(this)
            {
                FieldExpression = field,
                FieldName = GetFieldName(field),
                DisplayName = columnTitle,
                Size = size,
                SortOptions = sortOptions,
                TextAlign = textAlign
            };
            _columns.Add(columnOptions);
            return columnOptions;
        }

        public void AddDateColumn(Expression<Func<TModel, DateTime?>> field, int size, ColumnSortOptions sortOptions = ColumnSortOptions.Default)
        {
            var displayName = ExpressionHelper.GetPropertyDisplayName(field);
            var columnTitle = displayName ?? string.Empty;
            AddColumn(field, columnTitle, size + "px");
            DateRender(field);
        }

        public void AddPercentColumn(Expression<Func<TModel, decimal?>> field, int size, bool hidePrefix = false, ColumnSortOptions sortOptions = ColumnSortOptions.Default, ColumnTextAlign textAlign = ColumnTextAlign.Left)
        {
            AddColumn(field, size, sortOptions, textAlign);
            PercentRender(field, hidePrefix);
        }

        public void AddDecimalColumn(Expression<Func<TModel, decimal?>> field, string displayName, int size, DecimalFieldFormat format = DecimalFieldFormat.NoDigits)
        {
            if (string.IsNullOrEmpty(displayName))
                displayName = ExpressionHelper.GetPropertyDisplayName(field);

            var columnTitle = displayName ?? string.Empty;
            AddColumn(field, columnTitle, size + "px");
            DecimalRender(field, format);
        }

        public DynamicTreeColumnCustom<TModel> AddDynamicColumn(string fieldName, string columnTitle, int size, Action<DynamicTreeColumnCustom<TModel>> configuration)
        {
            var columnOptions = AddDynamicColumn(fieldName, columnTitle, size);
            configuration?.Invoke(columnOptions);

            return columnOptions;
        }

        public DynamicTreeColumnCustom<TModel> AddDynamicColumn(string fieldName, string columnTitle, int size, ColumnSortOptions sortOptions = ColumnSortOptions.Default)
        {
            var column = new DynamicTreeColumnCustom<TModel>(this)
            {
                FieldName = fieldName,
                DisplayName = columnTitle,
                Size = size + "px",
                SortOptions = sortOptions
            };
            _columns.Add(column);
            return column;
        }

        #region Data Builder

        private Type GetColumnEnumType(PropertyInfo propertyInfo)
        {
            if (!_enumColumns.ContainsKey(propertyInfo))
                return null;

            return _enumColumns[propertyInfo];
        }

        private Func<TModel, object, object> GetColumnRenderer(PropertyInfo propertyInfo) => !RenderColumns.ContainsKey(propertyInfo) ? null : RenderColumns[propertyInfo];

        internal string GetParentKey(object obj)
        {
            return _parentKeyMapping[obj];
        }

        internal object GetPropertyValue(object obj, PropertyInfo prop)
        {
            var value = prop.GetValue(obj);

            if (value != null && value.GetType() == typeof(string))
            {
                value = HttpUtility.HtmlEncode((string)value);
            }

            if (prop.PropertyType.IsEnum)
                value = ((Enum)value)?.GetDescription();

            var enumDefinition = GetColumnEnumType(prop);
            if (enumDefinition != null && value != null)
            {
                var enumValue = Enum.ToObject(enumDefinition, (int)value);
                value = EnumHelper.GetDescription(enumDefinition, enumValue);
            }

            var columnRenderer = GetColumnRenderer(prop);
            if (columnRenderer != null)
                value = columnRenderer((TModel)obj, value);

            return value;
        }

        internal Dictionary<string, Func<TreeItemModel<TModel>, object>> DynamicColumns => _dynamicColumns;

        public Expression<Func<TModel, string>> KeyColumnExpression { get; internal set; }
        public Expression<Func<TModel, int>> CountColumnExpression { get; internal set; }
        public Expression<Func<TModel, bool>> CanExpandColumnExpression { get; internal set; }
        // ReSharper disable once NotAccessedField.Local
        private Expression<Func<TModel, bool>> _keepWithoutValuedChildrenExpression;
        // ReSharper disable once NotAccessedField.Local
        private Expression<Func<TModel, int>> _nodeValueExpression;

        public PropertyInfo KeyMember { get; internal set; }
        public PropertyInfo TitleMember { get; internal set; }
        public PropertyInfo CountMember { get; internal set; }
        public PropertyInfo CanExpandMember { get; internal set; }
        internal PropertyInfo KeepWithoutValuedChildrenMember { get; set; }
        internal PropertyInfo NodeValueMember { get; set; }

        internal void EnumColumn<T>(Expression<Func<TModel, object>> column) where T : struct
        {
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            _enumColumns.Add(propInfo, typeof(T));
        }

        public void ColumnRender<T>(Expression<Func<TModel, T>> column, Func<TModel, T, object> renderFunc)
        {
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            RenderColumns.Add(propInfo, (model, value) => renderFunc(model, (T)value));
        }

        internal void EnumRender<T>(Expression<Func<TModel, object>> column) where T : struct
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new InvalidProgramException($"Cannot render {type.Name} type in dynamic tree");

            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            var compiledColumn = column.Compile();
            Func<TModel, object, object> renderFunc = (item, value) =>
            {
                var val = compiledColumn(item);

                return val != null ? EnumHelper.GetDescription(type, (T)val) : string.Empty;
            };

            RenderColumns.Add(propInfo, renderFunc);
        }

        public void DateRender(Expression<Func<TModel, DateTime?>> column)
        {
            var propertyName = ExpressionHelper.GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            var compiledColumn = column.Compile();
            Func<TModel, object, object> renderFunc = (item, value) =>
            {
                var val = compiledColumn(item);

                return val?.ToString("yyyy-MM-dd") ?? string.Empty;
            };

            RenderColumns.Add(propInfo, renderFunc);
        }

        public void PercentRender(Expression<Func<TModel, decimal?>> column, bool hidePrefix = false)
        {
            var propertyName = ExpressionHelper.GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            var compiledColumn = column.Compile();
            Func<TModel, object, object> renderFunc = (item, value) =>
            {
                var decimalValue = compiledColumn(item);
                if (RenderMode == TreeRenderMode.Data)
                    return decimalValue;

                return decimalValue.HasValue ? string.Format("{0:#,##0.0}" + " %", decimalValue) : string.Empty;
            };

            RenderColumns.Add(propInfo, renderFunc);
        }

        public void DecimalRender(Expression<Func<TModel, decimal?>> column, DecimalFieldFormat format)
        {
            var propertyName = ExpressionHelper.GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            var compiledColumn = column.Compile();
            Func<TModel, object, object> renderFunc = (item, value) =>
            {
                var val = compiledColumn(item);
                var formatTemplate = GetDecimalFormat(format);
                return val.HasValue ? string.Format(formatTemplate, val.Value) : string.Empty;
            };

            RenderColumns.Add(propInfo, renderFunc);
        }

        public void DynamicColumn(string name, Func<TreeItemModel<TModel>, object> renderFunc)
        {
            _dynamicColumns.Add(name, renderFunc);
        }

        public void KeyColumn(Expression<Func<TModel, string>> column)
        {
            KeyColumnExpression = column;
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            KeyMember = propInfo;
        }

        public void TitleColumn(Expression<Func<TModel, string>> column)
        {
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            TitleMember = propInfo;
        }

        public void CountColumn(Expression<Func<TModel, int>> column)
        {
            CountColumnExpression = column;
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            CountMember = propInfo;
        }

        public void CanExpandColumn(Expression<Func<TModel, bool>> column)
        {
            CanExpandColumnExpression = column;
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            CanExpandMember = propInfo;
        }

        public void KeepWithoutValuedChildrenColumn(Expression<Func<TModel, bool>> column)
        {
            _keepWithoutValuedChildrenExpression = column;
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            KeepWithoutValuedChildrenMember = propInfo;
        }

        public void NodeValueColumn(Expression<Func<TModel, int>> column)
        {
            _nodeValueExpression = column;
            var propertyName = GetFieldName(column);
            var propInfo = typeof(TModel).GetProperty(propertyName);
            if (propInfo == null)
                throw new InvalidProgramException($"Can't find property {column} in type {typeof(TModel)}");

            NodeValueMember = propInfo;
        }

        public IActionResult BuildData(string parentNodeId, Func<string, List<TModel>> dataSource)
        {
            if (KeyMember == null)
                throw new InvalidConfigurationException("Key column is not defined!");
            if (TitleMember == null)
                throw new InvalidConfigurationException("Title column is not defined!");
            if (CountMember == null)
                throw new InvalidConfigurationException("Count column is not defined!");

            var data = new List<object>();

            var sortColumns = _httpContext.Request.GetData(SortColumns);
            var sortDescriptions = GetSortDescriptions(sortColumns);

            var branchKeys = (parentNodeId ?? string.Empty).Split(new[] { ',' }, StringSplitOptions.None);

            foreach (var key in branchKeys)
            {
                // Code to time
                List<TModel> ds;

                //TODO: using (Timeline.Capture("Executing tree DS"))
                {
                    ds = dataSource(key);
                    ds = ds.BuildOrderBys(sortDescriptions).ToList();
                }

                //TODO: using (Timeline.Capture("Receiving tree DS data"))
                {
                    //read Request and sort ds
                    foreach (var dsItem in ds)
                    {
                        _parentKeyMapping.Add(dsItem, key);
                    }

                    data.AddRange(ds);
                }
            }

            return new TreeItemsResult(data, new TreeDataConverter<TModel>(this));
        }

        /// <summary>
        /// Creates data items for tree for a single level. Result is dynamic objects for every node with real data after all properties are rendered
        /// </summary>
        /// <param name="parentKey">Parent key</param>
        /// <param name="dataSource">Data source function</param>
        /// <param name="mapperFunc">Mapper between source data items and tree data item</param>
        /// <returns></returns>
        public object BuildDataObject(string parentKey, Func<string, List<TModel>> dataSource, Func<List<TModel>, object> mapperFunc = null)
        {
            var ds = dataSource(parentKey);
            foreach (var dsItem in ds)
            {
                _parentKeyMapping.Add(dsItem, parentKey);
            }

            object data = ds;
            if (mapperFunc != null)
            {
                data = mapperFunc(ds);
            }

#if DOTNET_CORE
            var settings = new JsonSerializerSettings { Converters = { new TreeDataConverter<TModel>(this) } };
            return JsonConvert.DeserializeObject(JsonConvert.SerializeObject(data, settings), settings);
#else
            var serializer = new JavaScriptSerializer();
            serializer.RegisterConverters(new[] { new TreeDataConverter<TModel>(this) });
            return serializer.Deserialize<dynamic>(serializer.Serialize(data));
#endif
        }

        /// <summary>
        /// Retrieves selected data items based on UI selection.
        /// Is used to retrieve selected data items on the backend in callback
        /// </summary>
        /// <param name="dataSource">Data source function, should equal to function that build the tree</param>
        /// <param name="selectedItems">Selected items with states (Checked and undetermined)</param>
        /// <param name="propagateSelectionToParents">Include items below selected nodes if node is fully checked. Otherwise returns only top selected items</param>
        /// <returns></returns>
        public List<TModel> GetSelectedItems(Func<string, List<TModel>> dataSource, IList<TreeSelectedItem> selectedItems, bool propagateSelectionToParents)
        {
            if (KeyMember == null)
                throw new InvalidConfigurationException("Key column is not defined!");
            if (CountMember == null)
                throw new InvalidConfigurationException("Count column is not defined!");

            if (!propagateSelectionToParents && selectedItems.Any(x => x.TreeSelectionState == UndeterminedStateValue))
                throw new InvalidOperationException($"Undetermined state is only valid for {nameof(propagateSelectionToParents)}");

            _executionContext.ItemKeysToLoad.Clear();
            _executionContext.ItemKeysToLoad.Add(string.Empty);

            var itemKeysChecked = new HashSet<string>();
            for (var i = 0; i < selectedItems.Count; i++)
            {
                var itemKey = selectedItems[i].Key;
                var itemState = selectedItems[i].TreeSelectionState;
                if (itemState == SelectedStateValue)
                {
                    itemKeysChecked.Add(itemKey);
                }
            }

            try
            {
                _executionContext.IsDataExtraction = true;

                var result = new List<TModel>();

                var keyFunc = KeyColumnExpression.Compile();
                var countFunc = CountColumnExpression.Compile();
                var canExpandFunc = CanExpandColumnExpression?.Compile();
                while (_executionContext.ItemKeysToLoad.Count > 0)
                {
                    if (!propagateSelectionToParents && result.Count == itemKeysChecked.Count) break;

                    var itemKey = _executionContext.ItemKeysToLoad[0];
                    _executionContext.ItemKeysToLoad.RemoveAt(0);

                    _executionContext.IsDataExtractionNodeFullySelected = itemKeysChecked.Contains(itemKey);

                    var data = dataSource(itemKey);

                    var isItemUnchecked = !string.IsNullOrEmpty(itemKey) && selectedItems.Any(x => x.TreeSelectionState == UncheckedStateValue && x.Key == itemKey);
                    if (isItemUnchecked)
                    {
                        foreach (var childItem in data)
                        {
                            var childKey = keyFunc(childItem);
                            if (itemKeysChecked.Contains(childKey))
                            {
                                itemKeysChecked.Remove(childKey);
                            }
                            var selectedChildItem = selectedItems.FirstOrDefault(x => x.Key == childKey);
                            if (selectedChildItem != null)
                            {
                                selectedChildItem.TreeSelectionState = UncheckedStateValue;
                            }
                            else
                            {
                                selectedItems.Add(new TreeSelectedItem(childKey, UncheckedStateValue));
                            }
                        }
                    }

                    var levelCheckedItems = data.Where(x => itemKeysChecked.Contains(itemKey) || itemKeysChecked.Contains(keyFunc(x))).ToArray();
                    Func<TModel, bool> canExpandFn = item => canExpandFunc?.Invoke(item) ?? countFunc(item) > 0;

                    foreach (var item in levelCheckedItems)
                    {
                        if (!propagateSelectionToParents)
                        {
                            _executionContext.ItemKeysToLoad.Remove(keyFunc(item));
                        }
                        else
                        {
                            if (!canExpandFn(item))
                            {
                                _executionContext.ItemKeysToLoad.Remove(keyFunc(item));
                            }
                        }
                    }

                    result.AddRange(levelCheckedItems);

                    var keysToLoadMore = data.Where(x => canExpandFn(x)).Select(keyFunc).ToList();
                    foreach (var key in keysToLoadMore)
                    {
                        _executionContext.ItemKeysToLoad.Add(key);

                        if (itemKeysChecked.Contains(itemKey) && propagateSelectionToParents)
                        {
                            itemKeysChecked.Add(key);
                        }
                    }
                }

                return result.Distinct().ToList();
            }
            finally
            {
                _executionContext.IsDataExtraction = false;
            }
        }

        /// <summary>
        /// Returns selected items for UI selection in mode with propagation of selection.
        /// Is used to render selection for UI tree in case of propagation selection to parents.
        /// Adds checked and undetermined items to selection.
        /// </summary>
        /// <param name="dataSource">Data source</param>
        /// <param name="selectedKeys">Selected keys</param>
        /// <returns></returns>
        public List<TreeSelectedItem> ExtendSelection(Func<string, List<TModel>> dataSource, IEnumerable<string> selectedKeys)
        {
            if (KeyMember == null)
                throw new InvalidConfigurationException("Key column is not defined!");
            if (CountMember == null)
                throw new InvalidConfigurationException("Count column is not defined!");

            var keysToSearch = new HashSet<string>(selectedKeys);
            var keysFound = new List<string>();

            var keysToLoad = new List<string> { null };

            var treeStructure = new List<TreeKeyItem>();
            var rootItem = new TreeKeyItem();
            treeStructure.Add(rootItem);

            var keyFunc = KeyColumnExpression.Compile();
            var countFunc = CountColumnExpression.Compile();
            var canExpandFunc = CanExpandColumnExpression?.Compile();

            while (keysToLoad.Any())
            {
                var nextLevelLoadKeys = new List<string>();
                foreach (var key in keysToLoad)
                {
                    var items = dataSource(key);

                    foreach (var item in items)
                    {
                        var dataKey = keyFunc(item);
                        var dataCount = countFunc(item);
                        var canExpand = canExpandFunc?.Invoke(item) ?? dataCount > 0;
                        if (canExpand)
                            nextLevelLoadKeys.Add(dataKey);

                        var treeItem = new TreeKeyItem
                        {
                            Key = dataKey,
                            ParentKey = key
                        };
                        treeStructure.Add(treeItem);

                        if (keysToSearch.Contains(dataKey))
                        {
                            treeItem.TreeSelectionState = SelectedStateValue;
                            keysFound.Add(dataKey);
                        }
                    }

                    if (keysFound.Count == keysToSearch.Count)
                    {
                        nextLevelLoadKeys = new List<string>();
                        break;
                    }
                }

                keysToLoad = nextLevelLoadKeys.ToList();
            }

            foreach (var treeItem in treeStructure)
            {
                if (string.IsNullOrEmpty(treeItem.Key))
                    continue;

                var parent = treeStructure.FirstOrDefault(x => x.Key == treeItem.ParentKey);
                if (parent != null)
                {
                    parent.Children.Add(treeItem);
                }
            }

            SelectNodes(rootItem);

            return treeStructure.Where(x => x.TreeSelectionState != UncheckedStateValue).Select(x => new TreeSelectedItem
            {
                Key = x.Key,
                ParentKey = x.ParentKey,
                TreeSelectionState = x.TreeSelectionState,
                ChildrenCount = x.Children.Count
            }).ToList();
        }

        private void SelectNodes(TreeKeyItem item)
        {
            foreach (var child in item.Children)
            {
                SelectNodes(child);
            }

            if (!item.Children.Any() || item.TreeSelectionState == SelectedStateValue)
                return;

            var checkStatus = UncheckedStateValue;
            var checkedCount = item.Children.Count(x => x.TreeSelectionState == SelectedStateValue);
            var hasIndefinite = item.Children.Any(x => x.TreeSelectionState == UndeterminedStateValue);
            if (checkedCount == item.Children.Count)
            {
                checkStatus = SelectedStateValue;
            }
            else if (hasIndefinite || checkedCount > 0)
            {
                checkStatus = UndeterminedStateValue;
            }
            item.TreeSelectionState = checkStatus;
        }

        #region Excel Export

        private string PrepareExcelText(string value)
        {
            return value.GetValueOrEmpty().Replace("<br/>", Environment.NewLine);
        }

        private List<TreeItemModel<TModel>> LoadTreeData(string parentId, Func<string, List<TModel>> dataSource)
        {
            var keyFunc = KeyColumnExpression.Compile();
            var countFunc = CountColumnExpression.Compile();
            var canExpandFunc = CanExpandColumnExpression?.Compile();

            var items = dataSource(parentId).Select(x => new TreeItemModel<TModel>
            {
                Data = x
            }).ToList();

            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var count = countFunc(item.Data);
                var canExpand = canExpandFunc?.Invoke(item.Data) ?? count > 0;
                if (canExpand)
                {
                    var key = keyFunc(item.Data);
                    var nextItems = dataSource(key);
                    var children = nextItems.Select(x => new TreeItemModel<TModel>
                    {
                        Data = x,
                        Parent = item
                    }).ToList();
                    items.InsertRange(i + 1, children);
                    item.Children.AddRange(children);
                }
            }

            return items;
        }

        private bool IsMultiLineText(string text)
        {
            if (string.IsNullOrEmpty(text))
                return false;

            return text.IndexOf("\n", StringComparison.Ordinal) > -1 || text.IndexOf("\r", StringComparison.Ordinal) > -1;
        }

        public byte[] ExportToXlsx(string name, string parentKey, Func<string, List<TModel>> dataSource, DynamicTreeExportOptions<TModel> options = null)
        {
            options = options ?? new DynamicTreeExportOptions<TModel>();

            var dataToExport = LoadTreeData(parentKey, dataSource);
            options.OnBeforeImport?.Invoke(this, dataToExport);

            var wb = new XSSFWorkbook();

            var sheet = wb.CreateSheet(name);

            var rowIndex = 0;
            var headerRow = sheet.CreateRow(rowIndex++);

            var properties = new List<PropertyInfo>();
            var forceWrapText = new List<bool>();
            for (var i = 0; i < Columns.Count; i++)
            {
                var column = Columns[i];
                properties.Add(typeof(TModel).GetProperty(column.FieldName));
                forceWrapText.Add(properties[i]?.GetCustomAttribute<ExportForceWrapTextAttribute>() != null);
                var rowCell = headerRow.CreateCell(i);

                var headStyle = (XSSFCellStyle)wb.CreateCellStyle();
                headStyle.SetFillForegroundColor(new XSSFColor(Color.FromArgb(128, 128, 128)));
                headStyle.FillPattern = FillPattern.SolidForeground;
                headStyle.Alignment = HorizontalAlignment.Center;
                if (forceWrapText[i] || options.WrapTextForMultilineText && IsMultiLineText(column.DisplayName))
                {
                    headStyle.WrapText = true;
                }
                rowCell.CellStyle = headStyle;

                rowCell.SetCellValue(PrepareExcelText(column.DisplayName));

            }

            Dictionary<string, short> dataFormats = new Dictionary<string, short>();
            var columnStyles = new List<XSSFCellStyle>();
            var columnWrapStyles = new List<XSSFCellStyle>();
            for (var i = 0; i < Columns.Count; i++)
            {
                var column = Columns[i];
                var columnStyle = (XSSFCellStyle)wb.CreateCellStyle();

                switch (column.TextAlign)
                {
                    case ColumnTextAlign.Left:
                        columnStyle.Alignment = HorizontalAlignment.Left;
                        break;
                    case ColumnTextAlign.Center:
                        columnStyle.Alignment = HorizontalAlignment.Center;
                        break;
                    case ColumnTextAlign.Right:
                        columnStyle.Alignment = HorizontalAlignment.Right;
                        break;
                    default:
                        throw new NotSupportedException($"Alignment {column.TextAlign.GetDescription()} is not supported!");
                }

                if (!string.IsNullOrEmpty(column.CustomExportFormatting))
                {
                    if (!dataFormats.ContainsKey(column.CustomExportFormatting))
                    {
                        dataFormats.Add(column.CustomExportFormatting, wb.CreateDataFormat().GetFormat(column.CustomExportFormatting));
                    }
                    columnStyle.DataFormat = dataFormats[column.CustomExportFormatting];
                }
                else
                {
                    if (column.DataType == typeof(decimal))
                        columnStyle.DataFormat = (short)BuiltinFormats.GetBuiltinFormat("#,##0");
                }

                columnStyles.Add(columnStyle);

                var columnWrapStyle = (XSSFCellStyle)wb.CreateCellStyle();
                columnWrapStyle.CloneStyleFrom(columnStyle);
                columnWrapStyle.WrapText = true;
                columnWrapStyles.Add(columnWrapStyle);
            }

            foreach (var data in dataToExport)
            {
                if (options.OnAddRow?.Invoke(data) == false) continue;

                var dataRow = sheet.CreateRow(rowIndex++);
                for (var i = 0; i < Columns.Count; i++)
                {
                    var column = Columns[i];
                    var propertyInfo = properties[i];
                    var rowCell = dataRow.CreateCell(i);
                    var value = propertyInfo != null ? GetPropertyValue(data.Data, propertyInfo) : _dynamicColumns[column.FieldName](data);

                    rowCell.CellStyle = columnStyles[i];
                    if (value != null)
                    {
                        var valueType = value.GetType();
                        if (valueType == typeof(decimal))
                        {
                            rowCell.SetCellValue((double)((decimal)value));
                        }
                        else if (valueType == typeof(double) || valueType == typeof(float))
                        {
                            rowCell.SetCellValue((double)value);
                        }
                        else if (valueType == typeof(DateTime))
                        {
                            rowCell.SetCellValue((DateTime)value);
                        }
                        else if (valueType == typeof(bool))
                        {
                            rowCell.SetCellValue((bool)value);
                        }
                        else
                        {
                            var text = Convert.ToString(value);
                            if (forceWrapText[i] || options.WrapTextForMultilineText && (text.IndexOf("\n", StringComparison.Ordinal) > -1 || text.IndexOf("\r", StringComparison.Ordinal) > -1))
                            {
                                rowCell.CellStyle = columnWrapStyles[i];
                            }
                            rowCell.SetCellValue(text);
                        }
                    }
                }
            }

            for (var i = 0; i < Columns.Count; i++)
            {
                var column = Columns[i];
                var widthAttribute = properties[i]?.GetCustomAttribute<ExportColumnWidthAttribute>();
                if (widthAttribute != null || column.ExportWidthPt.HasValue)
                {
                    sheet.SetColumnWidth(i, column.ExportWidthPt ?? widthAttribute.WidthPt);
                }
                else if (options.AutoSizeColumns)
                {
                    sheet.AutoSizeColumn(i);
                }
            }

            using (var ms = new MemoryStream())
            {
                wb.Write(ms);
                return ms.ToArray();
            }
        }

        #endregion

        #region Private Methods

        private SortDescription[] GetSortDescriptions(string sortString)
        {
            if (string.IsNullOrEmpty(sortString))
                return new SortDescription[] { };

            var sortColumns = sortString.Split('|').Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (!sortColumns.Any())
                return new SortDescription[] { };

            var sortDescriptions = new SortDescription[sortColumns.Count()];
            var i = 0;
            foreach (var sc in sortColumns)
            {
                var fieldSortAttributes = sc.Split('_').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                if (fieldSortAttributes.Length == 2)
                {
                    var key = fieldSortAttributes[0];
                    var direction = fieldSortAttributes[1] == "ASC" ? ListSortDirection.Ascending : ListSortDirection.Descending;
                    sortDescriptions[i] = new SortDescription(key, direction);
                }
                i++;
            }

            return sortDescriptions;
        }

        private static string GetDecimalFormat(DecimalFieldFormat format)
        {
            switch (format)
            {
                case DecimalFieldFormat.NoDigits:
                    return "{0:#,##0}";
                case DecimalFieldFormat.TwoDigits:
                    return "{0:#,##0.00}";
                case DecimalFieldFormat.KiloNoDigits:
                    return "{0:#,##0,}";
                case DecimalFieldFormat.KiloTwoDigits:
                    return "{0:#,##0,.00}";
                case DecimalFieldFormat.MillionNoDigits:
                    return "{0:#,##0,,}";
                case DecimalFieldFormat.MillionTwoDigits:
                    return "{0:#,##0,,.00}";
                default:
                    throw new NotSupportedException($"Format {format} is not supported!");
            }
        }

        #endregion

        #endregion
    }
}
