﻿using System.Collections.Generic;

namespace FirstQuad.DynamicTree.Server.Classes
{
    public class TreeItemModel<TModel>
    {
        private List<TreeItemModel<TModel>> _parentsOrSelf;

        public TModel Data { get; set; }
        public TreeItemModel<TModel> Parent { get; set; }
        public List<TreeItemModel<TModel>> Children { get; }

        public List<TreeItemModel<TModel>> ParentsOrSelf
        {
            get
            {
                if (_parentsOrSelf == null)
                {
                    _parentsOrSelf = new List<TreeItemModel<TModel>>();
                    var parent = this;
                    while (parent != null)
                    {
                        _parentsOrSelf.Add(parent);
                        parent = parent.Parent;
                    }
                }
                return _parentsOrSelf;
            }
        }

        public TreeItemModel()
        {
            Children = new List<TreeItemModel<TModel>>();
        }
    }
}
