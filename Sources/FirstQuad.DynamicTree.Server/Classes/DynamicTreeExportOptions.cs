﻿using System.Collections.Generic;
using FirstQuad.DynamicTree.Server.Interfaces;

namespace FirstQuad.DynamicTree.Server.Classes
{
    public delegate bool DynamicTreeExportOnAddRow<TModel>(TreeItemModel<TModel> item) where TModel : class;
    public delegate void DynamicTreeExportOnBefore<TModel>(IDynamicTree<TModel> tree, List<TreeItemModel<TModel>> data) where TModel : class;

    public class DynamicTreeExportOptions<TModel> where TModel : class
    {
        public DynamicTreeExportOptions()
        {
            WrapTextForMultilineText = true;
            AutoSizeColumns = true;
        }

        public DynamicTreeExportOnAddRow<TModel> OnAddRow { get; set; }

        public DynamicTreeExportOnBefore<TModel> OnBeforeImport { get; set; }

        public bool WrapTextForMultilineText { get; set; }

        public bool AutoSizeColumns { get; set; }
    }
}