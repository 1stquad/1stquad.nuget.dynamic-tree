﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
#if DOTNET_CORE
using Newtonsoft.Json;
#else
using System.Web.Script.Serialization;
using JsonIgnoreAttribute = System.Web.Script.Serialization.ScriptIgnoreAttribute;
#endif

namespace FirstQuad.DynamicTree.Server.Classes
{
#if DOTNET_CORE
    internal class TreeDataConverter<TDataModel> : JsonConverter where TDataModel : class
#else
    internal class TreeDataConverter<TDataModel> : JavaScriptConverter where TDataModel : class
#endif
    {
        private readonly List<PropertyInfo> _properties;
        private readonly DynamicTree<TDataModel> _treeBuilder;

        public TreeDataConverter(DynamicTree<TDataModel> treeBuilder)
        {
            _treeBuilder = treeBuilder;
            _properties = typeof(TDataModel).GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(p => p.GetCustomAttribute<JsonIgnoreAttribute>() == null).ToList();
        }

        private string FindParentKey(object obj)
        {
            return _treeBuilder.GetParentKey(obj);
        }

        private IDictionary<string, object> GetProperties(object obj)
        {
            var toSerialize = new Dictionary<string, object>();
            toSerialize.Add("Key", _treeBuilder.GetPropertyValue(obj, _treeBuilder.KeyMember));
            toSerialize.Add("ParentKey", FindParentKey(obj));
            toSerialize.Add("Title", _treeBuilder.GetPropertyValue(obj, _treeBuilder.TitleMember));
            var count = (int)_treeBuilder.GetPropertyValue(obj, _treeBuilder.CountMember);
            toSerialize.Add("Count", count);
            toSerialize.Add("CanExpand", _treeBuilder.CanExpandMember == null ? count > 0 : _treeBuilder.GetPropertyValue(obj, _treeBuilder.CanExpandMember));
            if (_treeBuilder.KeepWithoutValuedChildrenMember != null)
            {
                toSerialize.Add("KeepWithoutValuedChildren", _treeBuilder.GetPropertyValue(obj, _treeBuilder.KeepWithoutValuedChildrenMember));
            }
            if (_treeBuilder.NodeValueMember != null)
            {
                toSerialize.Add("NodeValue", _treeBuilder.GetPropertyValue(obj, _treeBuilder.NodeValueMember));
            }

            for (var i = 0; i < _properties.Count; i++)
            {
                var prop = _properties[i];
                var value = _treeBuilder.GetPropertyValue(obj, prop);
                if (!toSerialize.ContainsKey(prop.Name))
                {
                    toSerialize.Add(prop.Name, value);
                }
            }

            foreach (var dynamicFieldName in _treeBuilder.DynamicColumns.Keys)
            {
                var item = new TreeItemModel<TDataModel>
                {
                    Data = (TDataModel)obj
                };
                var value = _treeBuilder.DynamicColumns[dynamicFieldName](item);
                toSerialize.Add(dynamicFieldName, value);
            }

            return toSerialize;
        }

#if DOTNET_CORE
        public override void WriteJson(JsonWriter writer, object obj, JsonSerializer serializer)
        {
            var props = GetProperties(obj);
            writer.WriteStartObject();
            foreach (var keyValuePair in props)
            {
                writer.WritePropertyName(keyValuePair.Key);
                writer.WriteValue(keyValuePair.Value);
            }
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsAssignableFrom(typeof(TDataModel));
        }
#endif

#if !DOTNET_CORE
        public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
        {
            var toSerialize = new Dictionary<string, object>();
            toSerialize.Add("Key", _treeBuilder.GetPropertyValue(obj, _treeBuilder.KeyMember));
            toSerialize.Add("ParentKey", FindParentKey(obj));
            toSerialize.Add("Title", _treeBuilder.GetPropertyValue(obj, _treeBuilder.TitleMember));
            var count = (int)_treeBuilder.GetPropertyValue(obj, _treeBuilder.CountMember);
            toSerialize.Add("Count", count);
            toSerialize.Add("CanExpand", _treeBuilder.CanExpandMember == null ? count > 0 : _treeBuilder.GetPropertyValue(obj, _treeBuilder.CanExpandMember));
            if (_treeBuilder.KeepWithoutValuedChildrenMember != null)
            {
                toSerialize.Add("KeepWithoutValuedChildren", _treeBuilder.GetPropertyValue(obj, _treeBuilder.KeepWithoutValuedChildrenMember));
            }
            if (_treeBuilder.NodeValueMember != null)
            {
                toSerialize.Add("NodeValue", _treeBuilder.GetPropertyValue(obj, _treeBuilder.NodeValueMember));
            }

            for (var i = 0; i < _properties.Count; i++)
            {
                var prop = _properties[i];
                var value = _treeBuilder.GetPropertyValue(obj, prop);
                if (!toSerialize.ContainsKey(prop.Name))
                {
                    toSerialize.Add(prop.Name, value);
                }
            }

            foreach (var dynamicFieldName in _treeBuilder.DynamicColumns.Keys)
            {
                var item = new TreeItemModel<TDataModel>
                {
                    Data = (TDataModel)obj
                };
                var value = _treeBuilder.DynamicColumns[dynamicFieldName](item);
                toSerialize.Add(dynamicFieldName, value);
            }

            return toSerialize;
        }

        public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Type> SupportedTypes => new[] { typeof(TDataModel) };
#endif
    }
}