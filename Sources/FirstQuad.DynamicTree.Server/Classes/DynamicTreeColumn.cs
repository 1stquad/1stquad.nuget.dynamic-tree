using System;
using System.Linq.Expressions;

namespace FirstQuad.DynamicTree.Server.Classes
{
    public class DynamicTreeColumn<TModel, TValue> : DynamicTreeColumnBase<TModel> where TModel : class
    {
        public Expression<Func<TModel, TValue>> FieldExpression { get; set; }

        public void UseRenderer(Action<DynamicTreeColumnRenderArgs<TModel, TValue>> renderAction)
        {
            Tree.ColumnRender(FieldExpression, (model, value) =>
            {
                var arguments = new DynamicTreeColumnRenderArgs<TModel, TValue>
                {
                    Value = value,
                    DisplayValue = null,
                    Item = model
                };
                renderAction(arguments);
                return arguments.DisplayValue ?? arguments.Value;
            });
        }

        public DynamicTreeColumn(DynamicTree<TModel> tree): base(tree)
        {
            DataType = typeof (TValue);
            var nullableType = Nullable.GetUnderlyingType(DataType);
            if (nullableType != null)
            {
                DataType = nullableType;
            }
        }
    }
}