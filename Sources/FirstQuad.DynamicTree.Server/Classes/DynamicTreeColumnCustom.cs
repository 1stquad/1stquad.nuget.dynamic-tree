using System;

namespace FirstQuad.DynamicTree.Server.Classes
{
    public class DynamicTreeColumnCustom<TModel> : DynamicTreeColumnBase<TModel> where TModel : class
    {
        public void UseRenderer(Action<DynamicTreeColumnRenderArgs<TModel>> renderAction)
        {
            Tree.DynamicColumn(FieldName, model =>
            {
                var arguments = new DynamicTreeColumnRenderArgs<TModel>
                {
                    Value = null,
                    Item = model
                };
                renderAction(arguments);
                return arguments.Value;
            });
        }

        public DynamicTreeColumnCustom(DynamicTree<TModel> tree): base(tree)
        {
        }
    }
}