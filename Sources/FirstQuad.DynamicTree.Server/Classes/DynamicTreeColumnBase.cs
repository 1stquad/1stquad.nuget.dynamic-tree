using System;

namespace FirstQuad.DynamicTree.Server.Classes
{
    public class DynamicTreeColumnBase<TModel> where TModel : class
    {
        protected readonly DynamicTree<TModel> Tree;

        public DynamicTreeColumnBase(DynamicTree<TModel> tree)
        {
            Tree = tree;
        }

        public string FieldName { get; set; }
        public string DisplayName { get; set; }
        public string Size { get; set; }
        /// <summary>
        /// The maximum column width for an individual cell is 255 characters.
        /// Applicable at Excel export
        /// </summary>
        public int? ExportWidthPt { get; set; }
        public ColumnSortOptions SortOptions { get; set; }
        public ColumnTextAlign TextAlign { get; set; }
        public string CustomExportFormatting { get; set; }
        internal Type DataType { get; set; }
    }
}