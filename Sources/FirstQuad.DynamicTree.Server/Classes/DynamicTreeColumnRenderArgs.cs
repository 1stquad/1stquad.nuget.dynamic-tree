using System;

namespace FirstQuad.DynamicTree.Server.Classes
{
    public class DynamicTreeColumnRenderArgs<TModel, TValue> : EventArgs
        where TModel : class
    {
        public TValue Value { get; set; }
        public object DisplayValue { get; set; }
        public TModel Item { get; set; }
    }

    public class DynamicTreeColumnRenderArgs<TModel> : EventArgs 
        where TModel : class
    {
        public object Value { get; set; }
        public TreeItemModel<TModel> Item { get; set; }
    }
}