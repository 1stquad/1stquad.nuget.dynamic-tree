using System.Collections.Generic;
using System.Text;
using System.Net;
#if DOTNET_CORE
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
#else
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
#endif

namespace FirstQuad.DynamicTree.Server.Classes
{
    public class TreeItemsResult : ActionResult
    {
        private object Data { get; set; }

#if DOTNET_CORE
        private readonly JsonConverter _converter;

        public TreeItemsResult(List<object> data, JsonConverter converter)
#else
        private readonly JavaScriptConverter _converter;

        public TreeItemsResult(List<object> data, JavaScriptConverter converter)
#endif
        {
            _converter = converter;
            Data = data;
        }

#if DOTNET_CORE
        public override Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;

            var result = JsonConvert.SerializeObject(Data, new JsonSerializerSettings
            {
                Converters = new List<JsonConverter> { _converter }
            });

            response.StatusCode = (int)HttpStatusCode.OK;
            response.Headers[HeaderNames.ContentType] = "application/json";

            var data = Encoding.UTF8.GetBytes(result);
            return context.HttpContext.Response.Body.WriteAsync(data, 0, data.Length);
        }
#else
        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = "application/json";

            response.ContentEncoding = Encoding.UTF8;
            var serializer = new JavaScriptSerializer();
            serializer.RegisterConverters(new[] { _converter });
            serializer.MaxJsonLength = 2097152*10;
            if (Data != null)
            {
                string json = serializer.Serialize(Data);
                response.Write(json);
            }
        }
#endif
    }
}