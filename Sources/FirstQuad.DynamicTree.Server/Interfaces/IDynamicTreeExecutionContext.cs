using System.Collections.Generic;

namespace FirstQuad.DynamicTree.Server.Interfaces
{
    public interface IDynamicTreeExecutionContext
    {
        bool IsDataExtraction { get; set; }
        bool IsDataExtractionNodeFullySelected { get; set; }
        List<string> ItemKeysToLoad { get; }
    }
}