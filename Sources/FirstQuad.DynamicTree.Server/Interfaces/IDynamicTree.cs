﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FirstQuad.DynamicTree.Server.Classes;
#if DOTNET_CORE
using Microsoft.AspNetCore.Mvc;
#else
using System.Web.Mvc;
using IActionResult = System.Web.Mvc.ActionResult;
#endif

namespace FirstQuad.DynamicTree.Server.Interfaces
{
    public interface IDynamicTree<TDataModel> where TDataModel : class
    {
        void KeyColumn(Expression<Func<TDataModel, string>> column);
        void TitleColumn(Expression<Func<TDataModel, string>> column);
        void CountColumn(Expression<Func<TDataModel, int>> column);
        void CanExpandColumn(Expression<Func<TDataModel, bool>> column);
        void KeepWithoutValuedChildrenColumn(Expression<Func<TDataModel, bool>> column);
        void NodeValueColumn(Expression<Func<TDataModel, int>> column);
        DynamicTreeColumn<TDataModel, TValue> AddColumn<TValue>(Expression<Func<TDataModel, TValue>> field, int size, Action<DynamicTreeColumn<TDataModel, TValue>> configuration = null);
        DynamicTreeColumnCustom<TDataModel> AddDynamicColumn(string fieldName, string columnTitle, int size, Action<DynamicTreeColumnCustom<TDataModel>> configuration = null);
        DynamicTreeColumn<TDataModel, object> AddEnumColumn<TValue>(Expression<Func<TDataModel, object>> field, int size, ColumnSortOptions sortOptions = ColumnSortOptions.Default) where TValue: struct;
        void AddDateColumn(Expression<Func<TDataModel, DateTime?>> field, int size, ColumnSortOptions sortOptions = ColumnSortOptions.Default);
        void AddPercentColumn(Expression<Func<TDataModel, decimal?>> field, int size, bool hidePrefix = false, ColumnSortOptions sortOptions = ColumnSortOptions.Default, ColumnTextAlign textAlign = ColumnTextAlign.Left);
        IActionResult BuildData(string parentKey, Func<string, List<TDataModel>> dataSource);
        object BuildDataObject(string parentKey, Func<string, List<TDataModel>> dataSource, Func<List<TDataModel>, object> mapperFunc = null);
        List<TDataModel> GetSelectedItems(Func<string, List<TDataModel>> dataSource, IList<TreeSelectedItem> selectedItems, bool recursive = true);
        byte[] ExportToXlsx(string name, string parentKey, Func<string, List<TDataModel>> dataSource, DynamicTreeExportOptions<TDataModel> options = null);
        IDynamicTreeExecutionContext ExecutionContext { get; }
        List<TreeSelectedItem> ExtendSelection(Func<string, List<TDataModel>> dataSource, IEnumerable<string> selectedKeys);
        void ColumnRender<T>(Expression<Func<TDataModel, T>> column, Func<TDataModel, T, object> renderFunc);
        void DynamicColumn(string name, Func<TreeItemModel<TDataModel>, object> renderFunc);
        void AddDecimalColumn(Expression<Func<TDataModel, decimal?>> field, string displayName, int size, DecimalFieldFormat format = DecimalFieldFormat.NoDigits);
    }
}