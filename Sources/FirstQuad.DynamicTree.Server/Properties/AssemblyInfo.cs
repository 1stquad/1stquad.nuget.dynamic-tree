﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FirstQuad.DynamicTree.Server")]
[assembly: AssemblyDescription("FirstQuad.DynamicTree.Server server extensions. Allows to connect data sources with DynamicTree")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("1stQuad")]
[assembly: AssemblyProduct("FirstQuad.DynamicTree.Server")]
[assembly: AssemblyCopyright("1stQuad ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1fbcefe5-3980-4b07-8e49-d29ad6e7c133")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.1.3")]
