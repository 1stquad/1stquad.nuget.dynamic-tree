﻿using System;

namespace FirstQuad.DynamicTree.Server.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ExportColumnWidthAttribute: Attribute
    {
        public int WidthPt { get; }

        /// <summary>
        /// The maximum column width for an individual cell is 255 characters.
        /// </summary>
        /// <param name="widthPt"></param>
        public ExportColumnWidthAttribute(int widthPt)
        {
            WidthPt = widthPt;
        }
    }
}