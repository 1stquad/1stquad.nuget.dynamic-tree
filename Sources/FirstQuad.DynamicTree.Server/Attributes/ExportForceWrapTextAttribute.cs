﻿using System;

namespace FirstQuad.DynamicTree.Server.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ExportForceWrapTextAttribute : Attribute
    {
        
    }
}