﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DynamicTree.Net.Startup))]
namespace DynamicTree.Net
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
