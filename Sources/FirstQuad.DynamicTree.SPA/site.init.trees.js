﻿/* Part of nuget package */
site.addHtmlInitializer(function() {
    $(".tree-container", this).each(function () {
        var tree = $(this);
        if (!tree.data("dynamic-tree")) {
            tree.Tree();
        }
    });
});