﻿/* Part of nuget package */
site.addHtmlInitializer(function () {
    $(".dropdown-tree-picker", this).each(function () {
        var picker = $(this);
        picker.TreePicker();
    });
});