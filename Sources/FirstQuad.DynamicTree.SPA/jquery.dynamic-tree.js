﻿/* Part of nuget package */
$.fn.Tree = function () {
    "use strict";

    var treeDiv = $(this);
    var customJs = treeDiv.data("custom-js");
    var name = treeDiv.data("name");
    var treeColumn = treeDiv.data("tree-column");
    var formatRowJs = treeDiv.data("format-row-js");
    var formatColumnJs = treeDiv.data("format-column-js");

    var customJsFunction = new Function(customJs);
    var formatRowJsFunction = new Function(formatRowJs);
    var formatColumnJsFunction = new Function(formatColumnJs);

    var treeOptions = {
        autoRender: false,
        showCheckAll: treeDiv.data("show-check-all"),
        showCheckBoxes: treeDiv.data("show-check-boxes"),
        singleSelection: treeDiv.data("single-selection"),
        isReadOnly: treeDiv.data("read-only"),
        checkByClick: treeDiv.data("row-selection"),
        useForm: treeDiv.data("use-form"),
        pageSize: treeDiv.data("page-sizes"),
        maxItemsForAutoExpand: treeDiv.data("max-items-autoexpand"),
        autoExpandLevel: treeDiv.data("auto-expand-level"),
        allowColumnReordering: treeDiv.data("allow-column-reordering"),
        allowSorting: treeDiv.data("allow-sorting"),
        autoExpandAfterUpdate: treeDiv.data("auto-expand-after-update"),
        hideTotalCount: treeDiv.data("hide-total-count"),
        showContextMenu: treeDiv.data("show-context-menu"),
        stickyParentSelector: treeDiv.data("sticky-scroll-selector"),
        formatColumn: function (sender, data, fieldName) {
            if (!formatColumnJs) {
                if (data.CanExpand && fieldName == treeColumn) {
                    return data[fieldName] + " (" + data.Count + ")";
                }

                return data[fieldName];
            }

            return formatColumnJsFunction.apply(sender, [data, fieldName]);
        },
        formatRow: function (sender, e) {
            if (!formatRowJs) return;

            formatRowJsFunction.apply(sender, [sender, e.row, e.data]);
        }
    };

    if (customJs) {
        customJsFunction.apply(this, [treeOptions]);
    }

    var tree = new DynamicTree(treeDiv, treeOptions);
    if (name) {
        window[name] = tree;
    }

    if (treeDiv.data("auto-render-html")) {
        tree.renderTree(true);
    }
}