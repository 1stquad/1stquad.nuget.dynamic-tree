﻿/* Part of nuget package */
$.fn.TreePicker = function () {
    "use strict";
    var block = $(this);
    var input = block.children("input");
    var itemInfo = block.find(".tree-item-info");
    var clearBox = itemInfo.find(".tree-clear");
    var itemName = block.find(".tree-item-name");
    
    var initAction = block.data("tree-init-url");
    var infoAction = block.data("tree-info-url");
    var formatSelection = block.data("format-selection");
    var alwaysInitialize = block.data("always-initialize");
    var isDisabled = block.hasClass("user-disabled");
    var canClear = input.hasClass("select-clear") && !isDisabled;

    var content = $(this).find('.dropdown-content');
    block.data("content", content);

    if (!isDisabled) {

        var mode = block.data("mode").toLowerCase();

        if (mode == "hover") {
            block.hoverIntent(function() {
                    if (!block.hasClass("dropdown-disabled")) {
                        content.addClass('dropdown-content-active');
                        block.trigger("dropdown-show");
                    }
                },
                function() {
                    content.removeClass('dropdown-content-active');
                });
        } else if (mode == "click") {

            content.appendTo("body");

            block.on("remove", function() {
                content.remove();
            });

            block.on("click", function(e) {
                e.stopPropagation();
                if (!block.hasClass("dropdown-disabled")) {
                    block.trigger("dropdown-show");
                    block.jdropdown("show");
                    content.triggerHandler("show-content",
                    {
                        wndHeight: window.innerHeight,
                        position: content.position()
                    });
                }
            });
        }
    }

    var showClearBox = function () {
        if (canClear && content) {
            clearBox.removeClass("hidden");
        }
    };

    var treeDiv = null;
    var searchInput = null;
    var dynamicTree;
    if (content && content.length) {

        treeDiv = content.find(".dynamic-tree");
        searchInput = content.find(".tree-search-box > input");
        dynamicTree = treeDiv.data("dynamic-tree");
        if (!dynamicTree) {
            treeDiv.Tree();
            dynamicTree = treeDiv.data("dynamic-tree");
        }

        content.on("show-content", function (event, obj) {
            treeDiv.data("sender-position", obj.position);
            treeDiv.data("sender-window-height", obj.wndHeight);
            treeDiv.triggerHandler("resize-dropdown-content");
        });
    }

    var getFieldValues = function (item, formatFields) {
        var fieldValues = [];
        for (var i = 0; i < formatFields.length; i++) {
            var fieldValue = item[formatFields[i]];
            if (fieldValue) {
                fieldValues.push(fieldValue);
            }
        }

        return fieldValues;
    }

    var applyHint = function() {
        itemName.data("hint", itemName.text());
        itemName.tipTip({
            attribute: "data-hint"
        });
    }

    var lastRequestId = null;
    var updateText = function (text, uniqueId, hasValue) {
        itemInfo.removeClass("can-remove-value");
        if (hasValue) {
            if (canClear) {
                itemInfo.addClass("can-remove-value");
            }
            itemName.removeClass("grey-color");
            //itemInfo.addClass('padding-right-30');
        } else {
            itemName.addClass("grey-color");
            //itemInfo.removeClass('padding-right-30');
        }
        itemName.text(text);
        applyHint();
        if (uniqueId && infoAction) {
            lastRequestId = guid();
            var localRequestId = lastRequestId;
            var infoRequest = $.ajax({
                url: infoAction,
                data: {
                    uniqueId: uniqueId
                },
                method: "GET",
                cache: true
            });
            infoRequest.done(function (data) {
                if (localRequestId != lastRequestId || !data.text) return;
                itemName.text(data.text);
                applyHint();
            });
        }
    };

    var loadInitialValue = function () {
        if (parseInt(input.val())) {
            return $.ajax({
                url: initAction,
                data: {
                    value: input.val()
                },
                success: function (data) {
                    if (data.NotFound) {
                        updateText("[Invalid Selection]", data.UniqueId, true);
                        return;
                    }

                    showClearBox();
                    input.data("item", data);

                    var formatFields = formatSelection.split("|");
                    var fieldValues = getFieldValues(data, formatFields);
                    var name = fieldValues.length > 1 ? fieldValues.join(" - ") : fieldValues[0];
                    updateText(name, data.UniqueId, true);

                    input.trigger("item-initialized", data);
                }
            });
        }

        return $.Deferred().resolve({}).promise();
    }

    var loadPromise = loadInitialValue();

    input.on("tree-enable", function (e, isEnabled) {
        if (isEnabled) {
            block.removeClass("user-disabled dropdown-disabled");
        } else {
            block.addClass("user-disabled dropdown-disabled");
        }
    });

    var onItemSelected = function (e, nodeId) {
        var nodeData = dynamicTree.findData(nodeId);
        var item = null;
        if (nodeData && nodeData.dataItem) {
            item = nodeData.dataItem;
            var formatFields = formatSelection.split("|");
            var fieldValues = getFieldValues(item, formatFields);
            var name = fieldValues.length > 1 ? fieldValues.join(" - ") : fieldValues[0];
            updateText(name, item.UniqueId, true);
        }

        input.data("item", item);
        //force close popup
        itemName.parents(".dropdown-wrapper").find(".dropdown-content-active").removeClass("dropdown-content-active");
        input.val(nodeId.substr(4));
        input.trigger("item-selected", item);
        if (jQuery.fn.valid) input.valid();

        block.jdropdown("hide");
        showClearBox();
    };

    clearBox.on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        input.val("");
        updateText(input.attr("placeholder") + "", null, false);
        clearBox.addClass("hidden");
        input.trigger("item-removed", null);
    });

    block.on("dropdown-show", function () {
        if (loadPromise) {
            var loadContent = !input.val();
            dynamicTree.renderTree(loadContent);

            loadPromise.done(function(data) {
                if (data && data.Parents && data.Parents.length && treeDiv) {
                    dynamicTree.expandNodes(data.Parents, function() {
                        var node = dynamicTree.findNodeById(data.UniqueId);
                        if (node) {
                            treeDiv.off("dtree-checked", onItemSelected);
                            dynamicTree.navigateAndSelect(data.UniqueId);
                            treeDiv.on("dtree-checked", onItemSelected);
                        }
                    });
                } else {
                    if (data && !loadContent) {
                        dynamicTree.loadContent();
                    }
                }
            });
            loadPromise = null;
        } else {
            if (alwaysInitialize) {
                dynamicTree.refresh();
            }   
        }

        setTimeout(function () {
            try {
                searchInput.focus();
            } catch (error) {
            }
        }, 0);
    });

    if (searchInput && treeDiv) {
        var doSearch = function () {
            treeDiv.data("ajax-data-term", searchInput.val());

            dynamicTree.refresh();
        };

        searchInput.on("keydown change", function () {
            executeTimeout(doSearch, 500);
        });

        treeDiv.on("dtree-checked", onItemSelected);
        itemName.addClass("grey-color");
    }

    applyHint();
}